The Pylomer API Platform is a full-stack development project with a Google Cloud Endpoints back end and a Polymer/Dart front end.

This example implements a custom Craigslist search app. The web client inherits heavily from the Polymer-and-Dart codelab, which lives at http://dartlang.org/codelabs/polymer/

Docs for the Pylomer Platform are located at http://pylomer.com

commit updater test
