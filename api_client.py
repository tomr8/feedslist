#!/usr/bin/env python2

# Fix for `google` module from test/runner.py
# The built-in google module gets imported by default. This adds the SDK
#  google directory to the module search path, making google.appengine
#  available for import.
#
# TODO:
#   Determ how this works. Compare it to test/test_server.py
#
import os
import sys
sdk_path = os.path.expanduser('~/work/google-cloud-sdk/platform/google_appengine')
try:
    import google
    google.__path__.append("{0}/google".format(sdk_path))
except ImportError:
    pass

try:
    import protorpc
    protorpc.__path__.append("{0}/lib/protorpc-1.0/protorpc".format(sdk_path))
except ImportError:
    pass

sys.path.append("{0}/lib/endpoints-1.0".format(sdk_path))

import                      argparse
import                      apiclient
from apiclient import       discovery
import                      httplib2
import                      oauth2client
import                      pdb
import                      endpoints
from google.appengine.ext import ndb
from models import          Feed
from pprint import          pprint

# Here's a great way to exercise the live service without having to use the
# web browser.
#
# This file will live inside its project. It is called by simply running it on
# command line. Or it can be imported to call the various methods within
# a python interpreter session

# TODO: Make each method return, rather than print, the response. Test the
#        response for success. Or, better, set up a proper test harness.

# TODO: Create two different service objects, one with admin credentials
#        for adding regions and categories, and one with user credentials
#        for adding feeds and posts.

# TODO: Convert this into a test suite

# Set each of these variables as appropriate.
API_PORT = '8080'
#API_ROOT = 'https://craigslist-epd.appspot.com/_ah/api'
API_ROOT = 'http://localhost:%s/_ah/api' % API_PORT
API      = 'rest'
VERSION  = 'v1'

AUTH_REQUIRED = True

CLIENT_SECRETS_FILE = 'client_secret_console.json'
CREDENTIALS_FILE    = 'credentials.user.json'
EMAIL_SCOPE         = 'https://www.googleapis.com/auth/userinfo.email'

# Our service objects
categories  = None
regions     = None
feeds       = None
posts       = None

def get_credentials():
    """
    Gets Google API credentials or generates new ones
    if they don't exist or are invalid.
    """
    storage = oauth2client.file.Storage(CREDENTIALS_FILE)
    credentials = storage.get()

    flow = oauth2client.client.flow_from_clientsecrets(
            CLIENT_SECRETS_FILE,
            message="Missing client secrets file",
            scope = EMAIL_SCOPE)

    if credentials is None or credentials.invalid:
        flags = oauth2client.tools.argparser.parse_args()
        credentials = oauth2client.tools.run_flow(flow, storage, flags)
        storage.put(credentials)

    return credentials

def build_service(auth=AUTH_REQUIRED):
    # Build and set the global service object for interacting with the API.
    global categories, regions, feeds, posts

    if auth:
        credentials = get_credentials()
        http = httplib2.Http()
        http = credentials.authorize(http)
    else:
        http = None

    discoveryServiceUrl = '%s/discovery/v1/apis/%s/%s/rest' % (API_ROOT,
                                                               API,
                                                               VERSION)
    # TODO: may need to use `apiclient.discovery.build`
    service = discovery.build(API,
                              VERSION,
                              discoveryServiceUrl = discoveryServiceUrl,
                              http = http)

    # Set our global service vars
    categories = service.categories()
    regions    = service.regions()
    feeds      = service.feeds()
    posts      = service.posts()

# Create
def insert(service, entity):
    print "Insert ", repr(service), entity['id'] 

    # Does entity have ID?
    response = service.insert( id = entity['id'], body = entity).execute()
    pprint(response)

# Read
def get(service, item_id):
    print "Get ", repr(service), item_id
    # Fetch a single greeting and print it out.
    # 503
    response = service.get(id = item_id).execute()
    print '\"%s\"' % response

# List
def list(service):
    print "List ", repr(service) 
    # Fetch all entities and print them out.
    response = service.list().execute()
    pprint(response)

# Update
def update(service, item_id, attrs):
    print "Update ", repr(service), item_id
    response = service.update(
      id = item_id,
      body = attrs
    ).execute()
    pprint(response)

# Patch
def patch(service, item_id, attrs):
    """
    Update the specified properties of model.
    Properties in `attrs` set to `None` will not be modified.
    """
    print "Patch ", repr(service), item_id
    response = feeds.patch(
       id = item_id,
       body = attrs
    ).execute()
    pprint(response)

# execute
def execute(service, method, id=None, body=None, parent=None):
    print "\nExecuting: %s.%s with {id=%s}/{parent=%s} body=%s" % (service,
                                                                   method,
                                                                   id,
                                                                   parent,
                                                                   body)

    # Get a handle to the resource. Resource will then
    #  get called with various params to construct the
    #  query to be executed.
    request = eval("%s.%s" % (service, method))

    # Get key for parent
#   if parent:
#       parent = ndb.Key(kind, id)

    if method == 'get':
        if parent:
            request = request(id=id, parent=parent)
        else:
            request = request(id=id)

    elif method == 'insert':
        if parent:
            request = request(id=id, body=body, parent=parent)
        else:
            request = request(id=id, body=body)

    elif method == 'delete':
        if parent:
            request = request(id=id, parent=parent)
        else:
            request = request(id=id)

    elif method == 'list':
        if parent:
            request = request(parent=parent)
        else:
            request = request()

    try:
        response = request.execute()
    except endpoints.NotFoundException:
        print "Not found: ", repr(service), id, parent
    except apiclient.errors.HttpError, e:
        # HTTPError is a catch-all
        print "Error: ", e
    else:
        pprint(response)

    print '\n'


def main():
    # Test data
    testcat  = {'id': 'testcat',
                'title': 'Test category',
                'description': 'inserted with api_client'}

    testreg  = {'id': 'testreg',
                'title': 'Test region',
                'description': 'inserted with api_client',
                'subdomain': 'testsub'}

    testfeed  = {'id': 'testfeed',
                 'title': 'Feed AAA Title',
                 'description': 'inserted with api_client',
                 'category_id': 'testcat',
                 'region_id': 'testreg'}

    testpost  = {'id': 'testpost',
                 'parent': 'testfeed',
                 'title': 'Post AAA Title',
                 'description': 'inserted with api_client'}

    print "Building service...\n"
    build_service()

    print "Starting test...\n"
    testfeed_key = ndb.Key(Feed, 'testfeed')

    print "\nDeleting existing entities..."
    execute('posts'     , 'delete', id=testpost['id'], parent='testfeed')
    execute('feeds'     , 'delete', id=testfeed['id'])
    execute('regions'   , 'delete', id=testreg ['id'])
    execute('categories', 'delete', id=testcat ['id'])

    print "\nInserting entities..."
    execute('categories', 'insert', id=testcat ['id'], body=testcat)
    execute('regions'   , 'insert', id=testreg ['id'], body=testreg)
    execute('feeds'     , 'insert', id=testfeed['id'], body=testfeed)
    execute('posts'     , 'insert', id=testpost['id'], body=testpost, parent='testfeed')

    print "\nGetting post:"
    execute('posts'     , 'get'   , id=testpost['id'], parent=testfeed['id'])

    print "\nListing posts:"
    execute('posts'     , 'list'  , parent=testfeed['id'])

    print "\nTest complete..."

if __name__ == '__main__':
    main()
