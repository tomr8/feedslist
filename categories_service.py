from protorpc import                        remote
import                                      service
from models import                          Category as Model
from settings import                        settings


QUERY_FIELDS = ('limit', 'order', 'pageToken')
CREATE_FIELDS = tuple(set(Model.MESSAGE_FIELDS) - set(['id']))
USER_REQUIRED = settings.user_required

@service.api_root.api_class(resource_name='categories', path='categories')
class CategoriesService(remote.Service):
    """
    The Model API methods
    """

    @Model.method(http_method = 'PUT',
                  request_fields = CREATE_FIELDS,
                  path = '{id}',
                  user_required = USER_REQUIRED)
    def insert(self, my_model):
        """ Insert a Datastore entity
        """
        return service.insert(my_model)

    @Model.method(http_method = 'GET',
                  request_fields = ('id',),
                  response_fields = Model.MESSAGE_FIELDS,
                  path = '{id}',
                  user_required = USER_REQUIRED)
    def get   (self, my_model):
        """ Get a Datastore entity
        """
        return service.get   (my_model)

    @Model.method(http_method = 'POST',
                  path = '{id}',
                  request_fields = CREATE_FIELDS,
                  user_required = USER_REQUIRED)
    def update(self, my_model):
        """ Update a Datastore entity
        """
        return service.update(my_model)

    @Model.method(http_method = 'DELETE',
                  request_fields = ('id',),
                  response_fields = ('id', 'title'),
                  path = '{id}',
                  user_required = USER_REQUIRED)
    def delete(self, my_model):
        """ Delete a Datastore entity
        """
        return service.delete(my_model)

    @Model.query_method(query_fields = QUERY_FIELDS,
                        user_required = USER_REQUIRED)
    def list  (self, query):
        """ List Datastore entities
        """
        return service.list   (query)
