setup_env() {
    # First store existing environment in a variable
      CLIENT_LIB_NAME=clientlib
         PROJECT_NAME=craigslist-epd
              VERSION=1
              ACCOUNT=tmst08@gmail.com
          PROJECT_DIR=${PWD}
       CLIENT_LIB_DIR=${PROJECT_DIR}/web
  GENERATE_CLIENT_LIB=${PROJECT_DIR}/generate_client_library.py

  echo "Unset these vars by executing 'cleanup'."
}

usage() {
    echo -e "
    This script contains a number of functions and is not meant
    to be used directly but instead sourced into the working
    environment.

    Note: Must be run from project's root directory (use of PWD var)

    The normal sequence goes like this:
    $> source ./deploy.sh
    $> setup_env
    $> print_env
    $> deploy
    $> cleanup
    $> print_env

    Exported functions:
    [login, set_account, deploy, list_versions, delete_versions <versions>]."
}

print_env() {
    echo -e "
      CLIENT_LIB_NAME: ${CLIENT_LIB_NAME}
         PROJECT_NAME: ${PROJECT_NAME}
              VERSION: ${VERSION}
              ACCOUNT: ${ACCOUNT}
          PROJECT_DIR: ${PROJECT_DIR}
       CLIENT_LIB_DIR: ${CLIENT_LIB_DIR}
  GENERATE_CLIENT_LIB: ${GENERATE_CLIENT_LIB}
    "
}

login() {
    # launches browser
    gcloud auth login
}

list_accounts() {
    gcloud auth list
}

set_account() {
    gcloud config set account ${ACCOUNT}
}

push_sources() {
#   git push --all google	# old?
	git push origin master
}

deploy() {
#   pub build

    gcloud app deploy \
      --quiet \
      --promote \
      --verbosity=warning \
      --project=${PROJECT_NAME} \
      app.yaml \
      index.yaml

#   push_sources

    echo "Deployment complete."

    # Still wants dir even w/ flags.
    # Don't use appcfg.py
#   appcfg.py update ${PROJECT_FLAG} ${VERSION_FLAG} ${PROJECT_DIR}
}

list_versions() {
    gcloud app --project=${PROJECT_NAME} versions list
}

# specify version(s) from list_versions()
delete_versions() {
    gcloud app --project=${PROJECT_NAME} versions delete "$@"
}

cleanup_env() {
    echo "Unsetting env vars..."
    unset -f                \
        setup_env           \
        login               \
        set_account         \
        deploy              \
        list_versions       \
        delete_versions     \
        cleanup print_env

    unset                   \
        CLIENT_LIB_NAME     \
        PROJECT_NAME        \
        VERSION             \
        ACCOUNT             \
        PROJECT_DIR         \
        CLIENT_LIB_DIR      \
        GENERATE_CLIENT_LIB
    }

# Main
usage
setup_env
print_env
