#!/bin/bash

# This script should be sourced within an existing shell

# Note: to generate Dart client library, look at frontend/generate...

# TODO:
#   deploy new CL into Dart project
#   merge and delete generate_client_lib.sh

DARTIUM_HOME=/home/tom/work/dartium
API_NAME=rest
PROJECT_NAME=craigslist-epd
API_HOST=0.0.0.0

# Work with local app server
local_env() {
	      BASE_URL=http://localhost
	      API_PORT=8080	# back end

	       API_URL=${BASE_URL}:${API_PORT}
	  EXPLORER_URL=${API_URL}/_ah/api/explorer
	EXPLORER_FLAGS=--unsafely-treat-insecure-origin-as-secure=${API_URL}
	 DISCOVERY_URL=${API_URL}/_ah/api/discovery/v1/apis/${API_NAME}/v1/rest
}

# Work with deployed API
remote_env() {
	      BASE_URL=https://${PROJECT_NAME}.appspot.com
	      API_PORT=

	       API_URL=${BASE_URL}:${API_PORT}
	  EXPLORER_URL=${API_URL}/_ah/api/explorer
	EXPLORER_FLAGS=
	 DISCOVERY_URL=${API_URL}/_ah/api/discovery/v1/apis/${API_NAME}/v1/rest
}

# Display env vars
print_env() {
	echo -e "
      BASE_URL: $BASE_URL
      API_PORT: $API_PORT

       API_URL: $API_URL
  EXPLORER_URL: $EXPLORER_URL
EXPLORER_FLAGS: $EXPLORER_FLAGS
 DISCOVERY_URL: $DISCOVERY_URL
	"
}

# Start development server
api_server() {
  local_env
  dev_appserver.py                  \
    --host $API_HOST                \
    --port $API_PORT                \
    ./
}

if [ $1 ]; then
	if [ $1 == "remote" ]; then
		# Set up remote env
		remote_env
	elif [ $1 == "local" ]; then
		# Set up local env
		local_env
		api_server
		sleep 3
	fi
else
  # Invoked with no arg
	echo "Usage: <script> [local | remote]"
	echo "No args passed. (Re)setting remote env vars:"
	remote_env
	print_env
fi
