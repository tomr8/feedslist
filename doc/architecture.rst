=====================
Architectural details
=====================

The application can best be understood as a front end and a back end. Each contains a representation of Craigslist searches.

Front end
---------
The front end is a database management console similar to, but with considerably less functionality than, MySQL Workbench. It is a browser application using an MVC architecture implemented with Polymer web components using the Dart programming language. It currently runs only in Dartium, a special build of Chromium having the Dart V/M.

Note: Dart can be transpiled into Javascript which will run in any modern browser. Currently, however, the transpiled code is too large and unwieldy for practical use but this is expected to be fixed.

The application consists of a top-level HTML file (index.html) which links to a separate page for each of the primary collections of application objects (categories, regions, feeds and posts). Each of these collections is presented in its own single-page application (SPA). Each page, or SPA, allows items in a collection to be created, updated and deleted, all without requiring page reloads and fulfilling the primary objective in SPA architecture.

Back end
--------
The back end consists of REST APIs and data. The data is stored in Google’s Cloud Datastore. API is published by means of a Google App Engine application running on Google's highly scalable and available servers.

The data model
^^^^^^^^^^^^^^
Here's the data model for the Craisglist Search API:

.. figure:: model.png
   :align: center
   :alt: alternate text

   Data model for Craigslist searches

The API 
^^^^^^^
The back end stores its data in the Google Datastore, Google’s preferred non-relational (NoSQL) database. The Datastore contains collections of items, or entities. Each entity has a Kind, such as “Feed”, “Category” or “Region”. Each entity is a Python object attributes such as an ID, name and creation date specified for it at design time. Like any Python object, it also has methods providing an interface to its internal state. Messages sent from the client to the API cause these methods to be invoked.

The server
^^^^^^^^^^
The back end publishes, or enables the API to respond to requests, by means of a Python application (server) running on a machine which is accessible over the network. This server "listens" for and responds to requests from the client as specified by the API. The machine in this case runs in the Google Cloud. Google Cloud Endpoints intercepts requests from the client intended for the server, verifies their authenticity and passes them on to the server.

Cloud Endpoints provides a framework for accessing Datastore entities over plain HTTP connections. For example, the region for the "cars" feed can be viewed by visiting the following URL::

    http://feedslist.appspot.com/.../feeds/cars/region

The result appears in the browser in its JSON format::

    {
     "region": "north_san_diego",
    }

A similar request but with a request body might update the region for a feed. All application data can be created, updated and deleted in this manner, complicated somewhat by any authentication requirements. Herein lies the flexibility and convenience of REST APIs.

Endpoints Proto Datastore
^^^^^^^^^^^^^^^^^^^^^^^^^
EPD is a Cloud Endpoints helper library the Craigslist Search API makes use of. It takes care of much of the work of converting Datastore entities to and from their serialized JSON representation over the "wire". There is some concern that use of library might affect some queries so as to require query parameters rather than simple path elements. No examples of this are currently being observed.

Entity Structure
^^^^^^^^^^^^^^^^
In addition to the particular attributes of each entity Kind, all entities are created with five default attributes: ID, title, description, owner and date created. Attributes other than ID and date created may be later modifed. Title and description can be set to arbitrary values and serve as a limited form of entity metadata.

Entity IDs
^^^^^^^^^^
An entity's ID can be any sequence of alphanumeric characters. It functions as a key and must be unique within within the entity's Kind ("table" in SQL parlance). For instance, there can be no two feeds having the ID “bathroom_vanities”. But there *can* be both a feed and a search result having that ID.

It's rather unusual to set the key of an entity to a human-readable value rather than a generated sequence of alphanumerica characters. This allows easy recognition of an entity across the system and simplifies creating drop-down selection lists in web clients.
