=================
The Feedslist API
=================
Feedslist has a REST API back end. It can be accessed in a variety of ways including, but not limited to:

 - Browser address bar
 - Google API Explorer
 - Dartium web client
 - Client libaries
 - Python script
 - Cloud Datastore Console

Client libraries can be generated for other languages as well.

Access using the browser address bar
------------------------------------
Any unsecured or publicly-accessible parts of the API may be accessed in the same way as as a web page by entering its URL in the browser address bar. For example a search result might be displayed at the following URL::

    https://feedslist.appspot.com/api/posts/<post_id>

Access with API Explorer
------------------------
The Feedslist API can be accessed at its deployed URL using Google API Explorer. The URL for the deployed API might be::

    https://feedslist.appspot.com/_ah/api/explorer

API Explorer provides a great way to interact with the API and, uh, explore its full capabilities without the need for any installing any special client software. Plus it handles authentication, a particularly useful feature.

All of Google’s public APIs such as Drive, Sheets and YouTube are designed to to work with API Explorer. This means it receives excellent support from Google.

Here's an example of creating a new search feed for dining room sets in North San Diego County having a price between $650 and $1300 in new or barely-used condition:

.. figure:: api_explorer.png
   :align: center
   :alt: alternate text

   Creating a Feedslist search with API Explorer

Dartium web app
---------------
A web app which runs in Dartium, a special build of Chromium, provides the most convenient means of managing Feedslist entities. Other browsers are currently not supported mainly because they cannot run the application in an efficient way. There are plans to fix this.

Here's an example of creating the search feed described above using the Dartium web app:

.. figure:: dartium.png
   :align: center
   :alt: alternate text

   Creating a Feedslist search in Dartium

Client libraries
----------------
This is the preferred method of the accessing all Google APIs. Google API client libraries are published for Javascript and all other commonly used programming languages. The `Python client library`_ is particularly useful as it allows interaction with the deployed API inside a terminal window.

.. _Python client library: https://cloud.google.com/endpoints/docs/frameworks/python/access_from_python

The Feedslist API can also be accessed in the same manner as all public Google APIs by using the standard Javascript XMLHttpRequest object. `CORS`_ and authentication are supported.

.. _CORS: https://developers.google.com/api-client-library/javascript/features/cors.

Python client script
--------------------
Executing a search cannot be done by means of the deployed API. This is a limitation due to Craigslist blocking requests from the Google App Engine runtime. App Engine headers are added to any request from the runtime to an external server. Therefore a standalone Python script was developed which performs the search from the client machine.

The Python client script first accesses the API to determine which searches to perform. It does this by requesting any feeds which have the "Interval fetch" flag set. It then iterates over these feeds and queries the Craigslist server for search results.

The results (posts) for a search are then sent to the Feedslist server to be inserted into the Datastore by means of its API. The Feedslist server also sends any desired notifications to the owner of the feed via email or text message.

The Python script client can be run manually at any time or as a scheduled task such as a cron job on a Linux machine.

Google Cloud Datastore Console
------------------------------
The Datastore Console is only available to users given special project access inside the Google Cloud Console. It is mentioned here for the sake of completeness.
