============
Introduction
============
Feedslist is an RSS aggregator customized for Craigslist RSS feeds. It fills in some functionality missing from the Craigslist website:

 - When creating a search in your Craigslist account, you may specify only a single region and category for the search.
 - After creating a search, it cannot be thereafter modified
 - When a Craigslist listing expires, it disappears from the server along with its picture, full body and contact details. It may still exist in your feed aggregator, but with only a small portion of its original content.

Feedslist allows you to:

 - Define a search across multiple regions and categories
 - Edit a search
 - Be notified of results by text message

It also does something that few or no search aggregators do:

 - Save the image for a Craigslist search result
 - Edit a Craiglist search result's description and contact info

The Feedslist platform
^^^^^^^^^^^^^^^^^^^^^^
Feedslist incorporates current best practices in security and flexibility for your data. Data is stored on Google's hardened infrastructure and secured with industrial-strength authentication. Your data resides in the same Cloud NoSQL Datastore which Google uses both internally and for its public-facing applications.

Furthermore, all requests and responses are encrypted and transmitted with Google's SSL transport. No user without an authorized Gmail address can access your data. Period. OAuth2 means that Gmail authentication credentials are never provided to the Feedslist application but only to Google.
