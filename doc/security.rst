########
Security
########
The security picture consists of two main parts: authentication and authorization. Authentication means validating the identity of the user. Authorization means controlling what operations the user is able to perform. Operations consist of creating, reading, updating and deleting records from the system. 

**************
Authentication
**************
The centerpiece of the security picture consists of industrial-strength authentication provided as part of the Google App Engine platform. By default, all access is restricted users with valid Gmail addresses. Any unathenticated user is directed to a Google login screen. Any unknown user is denied access to application data. The user interacts only with Google when entering credentials.

Authentication is implemented on both the front and back ends. When loading the front end in a web browser, the user is redirected to a Google sign-in screen to provide valid Gmail credentials. Once entered, a token is returned and stored in the browser for use in making requests to the back end.

*************
Authorization
*************
The back end is provided the authenticated ID of the user making the request. With this ID, the request is authorized according to settings contained in a file, auth.yaml, which exists on the server. The auth.yaml file specifies which entities the user may access and what operations the user can perform on them.

The auth.yaml file implements a basic Role-Based Access Control (RBAC) scheme. A user is assigned one or more roles. A role has specific permissions in a given context. Entities and operations on entities are associated with required permissions. A user with a given role can perform only the operations allowed to that role.

The specifics of this are described in more detail below.

.. figure:: permissions.png
   :align: center
   :alt: alternate text

   Authorization settings

Roles
=====
The Pylomer API platform currently provides two roles: admin and user. These are implemented in the software as a list of user IDs for each role.

Admin role
----------
A user with the admin role may manage an entities having the admin permissions. For example, regions have admin permissions and thus may be created or deleted only by a user having the admin role.

User role
---------
A user with the user role may manage an entity having the user permissions. For example, feeds have user permissions and thus may be created or deleted only by a user having the user role.

Similar examples exist all over the web. For instance, an eBay user may manage his listings but may not manage the list of postal options available in the platform.

Permissions
===========
Each type of entity is likewise assigned a permissions level which can be either admin or user. Entities in the system can be cleanly divided into two main types: those (regions and categories) created by admininstrators and necessary to the functioning of the application, and those (feeds and posts) created by users in normal, daily operations.
Permissions are specified by means of specifying a required role to for an entity kind or operation. This has the effect of limiting access to users having the designated role.

Admin permissions
-----------------
Entities having importance to the functioning of the system are designated with admin permissions. Only users granted the admin role may modify or delete an entity so designated.

User permissions
----------------
Entities created by users are by default designated with user permissions. An entity so designated may be viewed, modifed or deleted only by the owner of the entity.
Configuring authorization

Configuring roles and permissions
=================================
Authorization settings are managed in the file auth.yaml. The structure of the file is fairly simple. Here's a sample auth.yaml::

 permissions:
  Region: admin
  Category: admin
  Feed: user
  Post: user
 roles:
  admin:
  - john@gmail.com
  - mary@gmail.com
  user:
  - syd@gmail.com
  - david@gmail.com
  - roger@gmail.com

Managing permissions and roles is simply a matter of modifying this file and deploying it to the App Engine runtime. The number of entity kinds (`Region`, `Category`, etc) and users (`john@gmail.com`, etc) may be increased without limit, but the list of roles (`user` and `admin`) is fixed. The resulting changes go into effect immediately. It should be noted that at no time are user credentials ever provided to the application.

Notice the two top-level sections: `permissions` and `roles`. `Permissions` lists each kind of entity available in the system and the role needed to manage it. `Roles` lists each `role` and the users to which it is granted. The `admin` role is specified for `Category` and `Region`. Likewise, the `user` role is specified for `Feed` and `Post`.

`john` and `mary` have the `admin` role and so may manage entities of kind `Region` and `Category`. Likewise, `syd`, `david` and `roger` have the `user` role and so may manage entities of kind `Feed` and `Post`.

By default, the `admin` role may not view or manage `Feed` or `Post` entities. An `admin` user must first sign out and sign back in using a different Gmail account having the `user` role. Also by default, a the `user` role may manage only those entities created by that user. Additionally, the `user` role may view, but not modify or delete, a `Region` or `Category`. This adheres to CMS conventions.

Some minor changes can be made to these rules without complicating the the logic. The Python script that reads this configuration file and applies the settings therein consists of about 30 lines of quite simple logic that can be easily modified to, say, allow the `admin` role to manage entities of any kind in the system or allow entities owned by a user to to be viewed by other users. A complete list of simple tweaks is planned.

Anything more involved is highly discouraged. With more complex rules comes greater opportunity for mistakes. Simplicity is the keyword for predictable access control.

App Engine authorization
========================
This is a separate set of authorizations related to the management of the App Engine platform, including the selection and deployment of application versions. Google's Identity and Access Management (IAM) can be used to tailor more sophisticated authorization schemes. More information can be found in the App Engine and Google Cloud documentation.
