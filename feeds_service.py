from copy import                            deepcopy
from protorpc import                        remote
import                                      service
from models import                          Feed as Model
from models import                          Query
from settings import                        settings


QUERY_FIELDS = ('limit', 'order', 'pageToken') + tuple(
        set(Model.MESSAGE_FIELDS) - set(
                # Fields to exclude from requests
                ['id', 'title', 'description', 'query_obj', 'status']))

CREATE_FIELDS = tuple(
        set(Model.MESSAGE_FIELDS) - set(['id']))

USER_REQUIRED = settings.user_required

@service.api_root.api_class(resource_name='feeds', path='feeds')
class FeedsService(remote.Service):
    """
    The Model API methods

    Fields in request and response messages default to all unless the
    respective _fields parameter is included in the method decoration.
    """
    @Model.method(http_method = 'PUT',
                  request_fields = CREATE_FIELDS,
                  response_fields = Model.MESSAGE_FIELDS,
                  path = '{id}',
                  user_required = USER_REQUIRED)
    def insert(self, my_model):
        my_model.query_obj = Query()
        return service.insert(my_model)

    @Model.method(http_method = 'GET',
                  request_fields = ('id',),
                  response_fields = Model.MESSAGE_FIELDS,
                  path = '{id}',
                  user_required = USER_REQUIRED)
    def get   (self, my_model):
        return service.get   (my_model)

    @Model.method(http_method = 'POST',
                  request_fields = CREATE_FIELDS,
                  path = '{id}',
                  user_required = USER_REQUIRED)
    def update(self, my_model):
        return service.update(my_model)

    @Model.method(http_method = 'PATCH',
                  request_fields = CREATE_FIELDS,
                  path = '{id}',
                  user_required = USER_REQUIRED)
    def patch(self, my_model):
        if hasattr(my_model, 'query_obj'):
            # Something in query_obj changed
            if my_model.from_datastore:
                # Otherwise will be caught in service.patch
                feed = Model.get_by_id(my_model.id)
                # Clone the feed's query_obj
                qobj = deepcopy(feed.query_obj)
                # Set the needed values
                for key, val in my_model.query_obj._values.iteritems():
                    setattr(qobj, key, val)
                # And replace query_obj
                my_model.query_obj = qobj
        return service.patch(my_model)

    @Model.method(http_method = 'DELETE',
                  request_fields = ('id',),
                  response_fields = ('id', 'title'),
                  path = '{id}',
                  user_required = USER_REQUIRED)
    def delete(self, my_model):
        return service.delete(my_model)

    @Model.query_method(query_fields = QUERY_FIELDS,
#                       collection_fields = Model.MESSAGE_FIELDS,
                        user_required = USER_REQUIRED)
    def list   (self, query  ):
        return service.list   (query  )
