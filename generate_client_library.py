#!/usr/bin/python

import glob
import json
import os
import re
import subprocess
import sys

# discovery doc filename will be rest.json.
# discovery doc Api class will be RestApi. Ugh.

API_NAME            = 'rest'	    # must match `name` in api_root.py
VERSION             = 'v1'
LOCALHOST           = 'localhost:8080'
REMOTEHOST          = 'https://craigslist-epd.appspot.com'
DISCOVERY_DOCS_DIR  = 'discovery'   # location of discovery doc
INSTALLED_DIR       = 'web'         # location for installed client library

#discovery_url = '<host>/_ah/api/discovery/v1/apis/rest/v1/rest'

def make_path(name, extension):
    """
    Generates the discovery doc path with different extensions, since
    `endpointscfg.py` generates it with ".discovery".
    """
    return os.path.join(DISCOVERY_DOCS_DIR,
                        '%s_%s.%s' % (API_NAME, name, extension))

def get_discovery_doc(name, host):
    """
    Generate discovery doc as `<api_name>-<version>.discovery` as
    given in the service class decoration

    `host`: string: ['LOCALHOST' or 'REMOTEHOST']

    Note: the generated discovery document will differ by host
    only in the values of `basePath`, `rootUrl` and `baseUrl`.
    """

    output_file = make_path(name, 'json')

    # Ex: http://localhost:8080/_ah/api/discovery/v1/apis/feedslist/v1/rest
    discovery_url = '%s/_ah/api/discovery/v1/apis/%s/%s/rest' % (
       host, API_NAME, VERSION)

    wget = ['wget', '-O', output_file, discovery_url]

    print "discovery_url: ", discovery_url
    print "output_file: ", output_file
    print "wget args: ", wget

    # The old-fashioned way
    subprocess.call(wget)

def generate_library(name, host):
    """ Generate the client library

        `host`: string: ['local' or 'remote']
    """
    generate_dart = [
            'generate.dart', 'files',
            '-i', DISCOVERY_DOCS_DIR,
            '-o', DISCOVERY_DOCS_DIR]

    print "generate.dart args: ", generate_dart
    subprocess.call(generate_dart)

    # Add [_local|_remote] to filename. Move.
    #
    # 'restapi.dart'
    src = os.path.join(DISCOVERY_DOCS_DIR,
		       os.path.normpath(   '%s.dart' % (API_NAME   ,     )))
    # 'clientlib.dart'
    dst = os.path.join(INSTALLED_DIR,		# 'clientlib.dart'
		       os.path.normpath('%s_%s.dart' % ('clientlib', name)))

    print "Rename, move library file from ", src, " to ", dst
    os.rename(src, dst)

    print "Don't forget to modify link to installed client library."

def main():
    """
    Still kind of hokey...
    """

    hosts = [('local' , LOCALHOST),
             ('remote', REMOTEHOST)]

    for name, host in hosts:
        print "\nGetting discovery document for ", name, "from ", host
        get_discovery_doc(name, host)
        print "\nGenerating client library for ", name, "from ", host
        generate_library (name, host)
        # clear discovery docs between calls to `generate.dart`
        [os.unlink(f) for f in glob.glob('%s/*.json' % DISCOVERY_DOCS_DIR)]

if __name__ == '__main__':
    main()
