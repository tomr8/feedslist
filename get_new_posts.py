#!/usr/bin/python

"""
Run as a standalone script in a console or as a cron job. Takes a
single (string) argument, feed ID, and retrieves the RSS for the feed.
Then it iterates over the entries in the downloaded RSS and creates a
new Post entity in the datastore for each entry. Finally, it loads the
`status` attribute of the Feed with statistics about this run of the client.

Modeled after api_client.py.
"""

'';                         import argparse
from bs4                    import BeautifulSoup
from datetime               import datetime
from copy                   import deepcopy
from apiclient              import discovery # NOTE: PYTHONPATH set in .bashrc
'';                         import feedparser
'';                         import httplib2
from HTMLParser             import HTMLParser
'';                         import json
'';                         import oauth2client  # BEFORE file, tools!
'';                         import oauth2client.file  # weird import required
'';                         import googleapiclient
from pprint                 import pprint
'';                         import requests
'';                         import pdb
from hashlib                import sha1
from oauth2client           import tools
'';                         import sys
#from google.appengine.api  import urlfetch
from urllib                 import urlencode
from urlparse               import urljoin
from urlparse               import urlsplit
import pdb


API_PORT = '8080'
API_ROOT = 'http://localhost:%s/_ah/api' % API_PORT
API_ROOT = 'https://craigslist-epd.appspot.com/_ah/api'
API_NAME = 'feedslist'
VERSION  = 'v1'

AUTH_REQUIRED = True

SECRETS_FILE        = 'client_secret_console.json'
CREDENTIALS_FILE    = 'credentials.user.json'

SCOPE               = 'https://www.googleapis.com/auth/userinfo.email'
USER_AGENT          = 'api_client/1.0'
OAUTH_DISPLAY_NAME  = 'My EPD Command-line Tool'

# Global service object
service = None;

def get_credentials():
    """
    Gets Google API credentials or generates new ones
    if they don't exist or are invalid.
    """
    storage = oauth2client.file.Storage(CREDENTIALS_FILE)
    credentials = storage.get()

    # TODO: When does the flow run? Or is `credentials` never invalid?
    #       What if the credentials are for a different user?
    if credentials is None or credentials.invalid:
        # Get client secret, etc. from downloaded file
        with open(SECRETS_FILE, 'r') as f:
            secrets = json.loads(f.read())
            # Now we have
            # {
            #    "installed":{
            #        "client_id":"104335...apps.googleusercontent.com",
            #        "project_id":"pylomer-epd",
            #        "auth_uri":"https://accounts.google.com/o/oauth2/auth",
            #        "token_uri":"https://accounts.google.com/o/oauth2/token",
            #        "auth_provider_x509_cert_url":
            #           "https://www.googleapis.com/oauth2/v1/certs",
            #        "client_secret":"58yvZeZQIOhBDBdCl7Czw-Pa",
            #        "redirect_uris":["urn:ietf:wg:oauth:2.0:oob",
            #                         "http://localhost"]
            #    }
            #}

        parser = argparse.ArgumentParser(
            description='Auth Sample',
            formatter_class=argparse.RawDescriptionHelpFormatter,
            parents=[oauth2client.tools.argparser])
        flags = parser.parse_args(sys.argv[1:])

        # Web server flow.
        # Pops a browser window to select the user
        flow = oauth2client.client.OAuth2WebServerFlow(
            client_id = secrets['installed']['client_id'],
            client_secret = secrets['installed']['client_secret'],
            scope=SCOPE,    # no scope in client secrets?
            user_agent=USER_AGENT,
            oauth_displayname=OAUTH_DISPLAY_NAME)
        credentials = oauth2client.tools.run_flow(flow, storage, flags)

        storage.put(credentials)

    return credentials

def build_service(auth=AUTH_REQUIRED):
    """
    Build and set the global service object for interacting with the API.
    Return service object.
    """
    global service

    if auth:
        credentials = get_credentials()
        # credentials has id_token attr containing `email`,
        # 'email_verified`, etc
        http = httplib2.Http()
        http = credentials.authorize(http)
    else:
        http = None

    discoveryServiceUrl = '%s/discovery/v1/apis/%s/%s/rest' % (API_ROOT,
                                                               API_NAME,
                                                               VERSION)
    # TODO: may need to use `apiclient.discovery.build`
    service = discovery.build(API_NAME,
                              VERSION,
                              discoveryServiceUrl = discoveryServiceUrl,
                              http = http)
class Condition:
    """
    EnumProperty of Query
    Note: duplicate of class in models.py
    """
    new       = 10
    like_new  = 20
    excellent = 30
    good      = 40
    fair      = 50
    salvage   = 60


# TODO: move MyFeed to own module
# TODO: Verify self.query_obj.get() still works
class MyFeed(dict):
    """ Class that takes a feed and adds some methods to it
    """
    def __init__(self, *args):
        dict.__init__(self, *args)

    def get_entries(self, region_id):
        """ Retrieve the feed and update the
            `source` attribute for the feed
        """

        url = self.region_url(region_id)

        # TODO: Verify time since last update greater than search interval
        print("\nFetching items for Feed '%s' from '%s'..." % (
                self.get('id'), url))

#       pdb.set_trace()
        # Careful with that axe, Eugene!
        # result is a <class 'requests.models.Response'>
        result = requests.get(url)
        if result.status_code == 200:
            # TODO: Just return result so .content, .text and .raw are
            #       available to the calling code
            source = result.text
        else:
          raise Exception("Fetch didn't succeed.")

        print "  Fetched source for Feed %s." % self.get('id')

        # TODO: verify `source` updated with current feeds
        # result.text is unicode, .entries a list
        entries = feedparser.parse(source).entries
        print("  Retrieved %s posts..." % len(entries))

        return entries

    def region_url(self, region_id):
        """ Return the Craigslist URLs to retrieve
            the feed from the given region
        """

        query = service.regions().get(id = region_id)
        region = query.execute()

        if not region['subdomain_only']:
            # need path sep
            reg_elem = "%s/" % region_id
        else:
            reg_elem = ""

        # Get query string for url
        #
        if self.get('query_obj') is None:
            # TODO: Is this ever the case?
            query_str = "?format=rss"
        else:
            query_str = "?%s" % self.query_params()

        template = "http://{0}.craigslist.org/search/{1}{2}{3}"
        # Ex: http://sandiego.craigslist.org/search/nsd/app?srchType=T&hasPic=1

        return template.format(region['subdomain'],
                               reg_elem,
                               self.get('category_id'),
                               query_str)

    def query_params(self):
        """
        Construct the URL parameters for this query

        Duplicates Query::__str__ in models.py
        """

        # NOTE: All booleans should be False by default. This prevents
        #       unnecessary KeyError exceptions

        # TODO: Verify self.query_obj.get() still works
        #       when `self` is subclass of `dict`
        #
        # help(dict.get):
        #     D.get(k[,d]) -> D[k] if k in D, else d.  d defaults to None.

        # Create a mapping object
        map = {'format': 'rss'}

        qobj = self.get('query_obj')

        qs = qobj.get('query_string')
        if qs:
            map['query'] = qs

        if qobj['srch_type']:
            map['srchType'] = 'T'

        if qobj['has_pic']:
            map['hasPic'] = '1'

        if qobj['posted_today']:
            map['postedToday'] = '1'

        if qobj['bundle_duplicates']:
            map['bundleDuplicates'] = '1'

        if qobj['search_nearby']:
            map['searchNearby'] = '1'

        dist = qobj.get('search_distance')
        if dist:
            map['searchDistance'] = dist

        postal = qobj.get('postal')
        if postal:
            map['searchDistance'] = postal

        # NOTE: min or max == 0 will be ignored
        #
        min = qobj.get('min_price')
        if min:
            map['searchDistance'] = min

        max = qobj.get('max_price')
        if max:
            map['searchDistance'] = max

        condition = qobj.get('condition')
        if condition:
            # Dereference the enum values into a new list
            condition = [eval("Condition.%s" % c) for c in condition]
            map['condition'] = condition

        if qobj['search_nearby']:
            map['searchNearby'] = '1'

        # `doseq = True` returns each (condition) list
        # element as a separate key-value pair in URL
        return urlencode(map, True)

def url_to_id(url):
    """
    Use a SHA1 hash to convert a URL into a datastore id
    """
    # TODO: Account for cross-listing between regions and
    # possibly other considerations, such as date published
    id = sha1(url).hexdigest()    # + '.html'
    return id

def make_dt(timestr):
    """
    Convert string into `datetime` object
    """
    return datetime.strptime(
        # TODO: Compute "-07:00" based on region, daylight savings
        #       See: naive time
        timestr, "%Y-%m-%dT%H:%M:%S-07:00")

class Post:
    """
    Convenience class to enable simpler dot notation
    """
    pass



def main():
    global service

    build_service()

    # Instantiate these before looping
    #
    posts_service = service.posts()
    regions_service = service.regions()
    feeds_service = service.feeds()

    # prints the current time in a nice format upon invokation
    now = lambda: datetime.now().strftime("%b %d %Y %I:%M%p")

    # TODO: filter on `interval_fetch` attr
    feeds = feeds_service.list().execute()['items']

    for feed in feeds:
        # TODO: Create a new MyFeed from feed
        feed = MyFeed(feed)

        # TODO: remove after filtering above
        if not feed['interval_fetch']:
            continue

        print("\nStarting run of feed %s at %s" % (feed['id'], now()))

        # DEBUG
#       f = open('feed.rss', 'r')
#       source = f.read()

        num_posts = 0
        failed_inserts = 0
        skipped_inserts = 0

        for region_id in feed['region_ids']:
            entries = feed.get_entries(region_id)

            num_posts += len(entries)

            for entry in entries:
                # Add the Post objects to the datastore
                post_id = url_to_id(entry.link)

                # TODO: install Google API client on host machine
                # Has it already been added?
                try:
                    post = posts_service.get(parent=feed['id'],
                                             id=post_id).execute()
                except googleapiclient.errors.HttpError: # <HttpError 404 ...>
                    # Post not found 
                    #
                    # Note: Another HttpError, such as entity owned by another
                    # another user, might cause failure later when attempting
                    # to insert it
                    pass
                else:
                    # Post exists
                    skipped_inserts += 1
                    continue

                hp = HTMLParser()   # To handle ER in title

                # Populate a new post
                post = Post()
                post.id          = post_id
                post.title       = hp.unescape(entry.title) # convert ER to '$'
                post.description = hp.unescape(entry.summary_detail['value'])
#               post.owner       = endpoints.get_current_user()
                post.parent      = feed['id']
                post.link        = entry.link
                # TODO: remove as has no use
#               post.source      = entry  # serialized Python dict (pickle)
                post.str_published = entry.published
                post.str_updated = entry.updated

                # Add image to datastore if it exists
                # Example image URL:
                #  https://images.craigslist.org/00l0l_9vguojAqFWX_300x300.jpg
                # TODO: Don't try this when deployed!
                if hasattr(entry, 'enc_enclosure'):
                    img_resource = entry['enc_enclosure']['resource']
                    post.img_link = img_resource    # nice for emailing

                    # Get image data - Careful with that axe, Eugene
                    img = requests.get(img_resource)

                    # Both urlfetch and requests have status_code
                    if img.status_code == 200:
                        # TODO: img.raw might not need encoding
                        # base64 encoded in model class (Alias property)
                        post.photo = img.content.encode('base64')   # [.text | .content]
                    else:
                        # Fetch failed
                        # TODO: Set post.photo to "red dot" byte sequence.
                        # Or create custom generic logos depending on category
                        pass

                # Get link to phone number from description
                try:
                    soup = BeautifulSoup(post.description, 'html.parser')
                except AttributeError:
                    # 'str' object has no attribute 'text'
                    # post.description not valid HTML?
                    soup = BeautifulSoup(post.description)

                if soup.a and hasattr(soup.a, 'href'):

                    # deepcopy so can delete (decompose) DOM element
                    # from descript without destroying showcontact
                    showcontact = deepcopy(soup.a['href'])
                    post.phone = urljoin(
                            'http://' + region['subdomain'] + '.craigslist.org',
                            showcontact)

                    # Remove showcontact node from description
                    # Note: (and any other links)
                    while soup.a:
                        # might take 2 or more passes
                        soup.a.decompose()

                    # TODO: DRY this (also set above)
                    post.description = str(soup)

                else:
                    # No 'href'
                    post.phone = ""

                # TODO: Get email address from 'replylink' DOM node
                # Download original post and scrape the replylink
                post.email = ""

                query = posts_service.insert(parent=post.parent,
                                             id=post.id,
                                             # Gotta love Python!
                                             body=post.__dict__)
                try:
                    result = query.execute()
                except Exception, e:
                    # Probably googleapiclient.errors.HttpError:
                    # <HttpError 403 - entity owned by another user>
                    print "Unable to add new post: ", e
                    failed_inserts += 1

        # Done inserting new posts for feed. Update the feed's status.
        status = "Retrieved %s new posts on %s. Failed to insert %s posts. Skipped %s." % (
                num_posts,
                now(),
                failed_inserts,
                skipped_inserts)

        query = feeds_service.update(
            id = feed['id'],
            body = {
                'id': feed['id'],
                'status': status})
        try:
            result = query.execute()
            # TODO: This succeeds but feed status is not updated???
            print("\nFeed status: %s" % status)
        except:
            # Probably 503
            print("\nFailed to update feed's status: %s" % status)


if __name__ == '__main__':
    main()
