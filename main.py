import endpoints
from categories_service import CategoriesService
from    regions_service import    RegionsService
from      feeds_service import      FeedsService
from      posts_service import      PostsService

# Use of endpoints.api_server is the same for APIs created with or without
# endpoints-proto-datastore.
application = endpoints.api_server([
        CategoriesService,
           RegionsService,
             FeedsService,
             PostsService
    ],
    # "`restricted` possible upcoming feature to securely restrict
    # access to the API. Access will always be allowed in dev_appserver
    # and under unit tests."
    # NOTE: The above is in error. Access denied in unit tests.
    # If True or unset, the API will only be allowed to serve to
    # Google's API serving infrastructure once deployed.  Set to False to
    # allow other clients.  Under dev_appserver, all clients are accepted.
    # NOTE: This prevents discovery neither locally nor deployed
    restricted = False)
