#!/usr/bin/python2.7

"""
Migrate datastore entities after change in model(s)

For use from within interactive console

See api_client.py for example on using HTTP interface

To be pasted directly into runtime console
"""

import os
import sys

#from google.appengine.api import memcache
#from google.appengine.api import mail
#from google.appengine.api import urlfetch

sdk_path = os.path.expanduser('~/work/google-cloud-sdk/platform/google_appengine')

try:
    import google
    google.__path__.append("{0}/google".format(sdk_path))
except ImportError:
    pass

try:
    import protorpc
    protorpc.__path__.append("{0}/lib/protorpc-1.0/protorpc".format(sdk_path))
except ImportError:
    pass

sys.path.append("{0}/lib/endpoints-1.0".format(sdk_path))

import pdb
from pprint import pprint
import copy
from google.appengine.ext import ndb
from models import Feed

def migrate():
    feeds = Feed.query().fetch()
    print "Deleting feed regions..."
    for feed in feeds:
        try:
            # Must access region in this way since
            # Feed::region no longer exists in model?
            del feed._properties['region']
            feed.put()
        except KeyError:    # Not AttributeError?
            # Not all feeds have a region at this point
            print "Feed has no region"
    # see if it worked
    print "printing feed regions..."
    for feed in feeds:
        try:
            pprint(feed._properties['region'])
        except KeyError:    # Not AttributeError?
            print "Feed has no region"
