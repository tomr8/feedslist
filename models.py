"""
Interacting with remote console:

from models import Feed, Post

key = ndb.Key(Feed, 'jobs_sd_software') # Create a key for query
query = Post.query(Post.feed == key)    # Create query
jobs = query.fetch()                    # Fetch the posts whose feed is jobs
jobs_by_date = sorted(
        jobs, key=lambda job: job.published) # Sort by date published
jobs_by_date[-1]                        # Print the most recent job
"""

from copy                           import deepcopy
from datetime                       import datetime
pass;                               import endpoints
from endpoints_proto_datastore.ndb  import EndpointsAliasProperty
from endpoints_proto_datastore.ndb  import EndpointsModel
pass;                               import feedparser
from HTMLParser                     import HTMLParser
from google.appengine.api           import images
pass;                               import logging
from protorpc                       import messages
from google.appengine.ext.ndb       import msgprop
from google.appengine.ext           import ndb
pass;                               import os
pass;                               import pdb
from hashlib                        import sha1
from urllib                         import urlencode
from google.appengine.api           import urlfetch
from urlparse                       import urljoin
from urlparse                       import urlsplit


def strptime(value):
    """
    Return a datetime object from given string
    """
    if not isinstance(value, basestring):
        raise TypeError('Date must be a string')
    return datetime.strptime(value[:-6], "%Y-%m-%dT%H:%M:%S")

def strftime(dt):
    """
    Return a string from given datetime object
    """
    if dt is not None:
        return dt.strftime("%a, %b %d, %Y %H:%M")   # UTC?

class Category(EndpointsModel):
    """
    Property of Feed
    """

    DEFAULT_ORDER = 'id,'  #,title,description'

    MESSAGE_FIELDS = ('id', 'title', 'description')

    _message_fields_schema = MESSAGE_FIELDS

    title = ndb.StringProperty()
    description = ndb.StringProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)
    owner = ndb.UserProperty()

    def IdSet(self, value):
        if not isinstance(value, basestring):
            raise TypeError('ID must be a string')
        self.UpdateFromKey(ndb.Key(Category, value))

    @EndpointsAliasProperty(setter=IdSet, required=True)
    def id(self):
        if self.key is not None:
            return self.key.string_id()

#   @EndpointsAliasProperty(setter=EndpointsModel.OrderSet, default=DEFAULT_ORDER)
    @EndpointsAliasProperty(setter=EndpointsModel.OrderSet)
    def order(self):
        return super(Category, self).order

# Enforce the following set of values for condition
class Condition(messages.Enum):
    """
    EnumProperty of Query
    """
    new       = 10
    like_new  = 20
    excellent = 30
    good      = 40
    fair      = 50
    salvage   = 60

class Query(EndpointsModel):
    """
    StructuredProperty of Feed

    If postal then no searchNearby

    Example search query (Kors watch in jewelry) with all options:
    https://sandiego.craigslist.org/search/nsd/jwa?
        query=Kors&
        srchType=T&
        hasPic=1&
        postedToday=1&
        bundleDuplicates=1&
        searchNearby=2&
        search_distance=20&
        postal=92026&
        min_price=100&
        max_price=500&
        condition=10&
        condition=20&
        ...
        condition=60
    """

    # TODO: Couple with category as the options vary by category
    #       Or just add all options here
    #
    _message_fields_schema = ('query_string', 'srch_type', 'has_pic',
                              'posted_today', 'bundle_duplicates',
                              'search_nearby', 'search_distance', 'postal',
                              'min_price', 'max_price', 'condition')

    query_string      = ndb.StringProperty()
    srch_type         = ndb.BooleanProperty(default = False)
    has_pic           = ndb.BooleanProperty(default = False)
    posted_today      = ndb.BooleanProperty(default = False)
    bundle_duplicates = ndb.BooleanProperty(default = False)
    search_nearby     = ndb.BooleanProperty(default = False)
    search_distance   = ndb.IntegerProperty(
            validator = lambda prop, value: min(value, 200))
    postal            = ndb.StringProperty()
    min_price         = ndb.IntegerProperty()
    max_price         = ndb.IntegerProperty()
    condition         = msgprop.EnumProperty(Condition, repeated = True)


class Region(EndpointsModel):
    """
    Something associated with zero or more Feeds
    """
    DEFAULT_ORDER = 'id,'  #,title,description'

    MESSAGE_FIELDS = ('id', 'title', 'description', 'subdomain', 'subdomain_only')

    # Default fields in message when not overridden by method decorator
    _message_fields_schema = MESSAGE_FIELDS

    # TODO: set default values?
    # NOTE: `required` necessitates including them in `list()`
    #       EPD forum: "It's not a bug"
    title = ndb.StringProperty()
    description = ndb.StringProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)

    # URL fragment that comes before domain ('sandiego', etc)
    subdomain = ndb.StringProperty()

    # Whether a specific region exists for this subdomain. Or it's "all".
    # Example: sandiego.craigslist.org/search[/nsd]/mcy
    subdomain_only = ndb.BooleanProperty(default = False)

    # Represented in the ProtoRPC message schema as a message field - a field
    # whose value is itself a message
    owner = ndb.UserProperty()

    # TODO: Add owner_id attribute to allow `owner` as query param to `list()`

    # Setter to be used by the helper property `id`, which we are
    # overriding here. See endpoints_proto_datastore
    # custom_alias_properties example
    def IdSet(self, value):
        if not isinstance(value, basestring):
            raise TypeError('ID must be a string')
        # Call UpdateFromKey, which each of EndpointsModel.IdSet and
        # EndpointsModel.EntityKeySet use, to update the current entity
        # using a datastore key. This method sets the key on the current
        # entity, attempts to retrieve a corresponding entity from the
        # datastore and then patch in any missing values if an entity is
        # found in the datastore.
        self.UpdateFromKey(ndb.Key(Region, value))

    @EndpointsAliasProperty(setter=IdSet, required=True)
    def id(self):
        if self.key is not None:
            return self.key.string_id()

#   @EndpointsAliasProperty(setter=EndpointsModel.OrderSet, default=DEFAULT_ORDER)
    @EndpointsAliasProperty(setter=EndpointsModel.OrderSet)
    def order(self):
        return super(Region, self).order


class Notification(messages.Enum):
    """
    Designate the medium for notifications (in Feed, etc)
    """
# TODO: See https://.../docs/python/ndb/entity-property-reference.
#   Also epd/ndb/utils.py EnumPropertyToProto
    NONE  = 0
    EMAIL = 1
    TEXT  = 2
    BOTH  = 3


class Feed(EndpointsModel):
    """
    Encapsulates everything needed for a query
    """

    DEFAULT_ORDER = 'id,'

    # Used in service class to decorate methods
    MESSAGE_FIELDS = ('id', 'title', 'description', 'category_id', 'region_ids',
                      'query_obj', 'interval_fetch', 'notification', 'status')

    # Default fields in message when not overridden by method decorator
    _message_fields_schema = MESSAGE_FIELDS

    title          = ndb.    StringProperty()
    description    = ndb.    StringProperty()
    created        = ndb.  DateTimeProperty(auto_now_add=True)
    owner          = ndb.      UserProperty()
    category       = ndb.       KeyProperty(Category, required=True)
    interval_fetch = ndb.   BooleanProperty(default=False) # checked by client
    query_obj      = ndb.StructuredProperty(Query)         # 'query' taken
    regions        = ndb.       KeyProperty(Region, repeated=True)
    notification   = msgprop. EnumProperty(Notification)
    status         = ndb.      TextProperty()              # results of last fetch

    def IdSet(self, value):
        if not isinstance(value, basestring):
            raise TypeError('ID must be a string')
        self.UpdateFromKey(ndb.Key(Feed, value))

    @EndpointsAliasProperty(setter=IdSet, required=True)
    def id(self):
        if self.key is not None:
            return self.key.string_id()

#   @EndpointsAliasProperty(setter=EndpointsModel.OrderSet, default=DEFAULT_ORDER)
    @EndpointsAliasProperty(setter=EndpointsModel.OrderSet)
    def order(self):
        return super(Feed, self).order

    ################# Category ######################
    def CategoryIdSet(self, value):
        if not isinstance(value, basestring):
            raise TypeError('ID must be a string')
        # Verify the category exists
        category = Category.get_by_id(value)
        if not category:
            raise endpoints.NotFoundException(
                    "No category found with key '%s'" % value)
        self.category = category.key

    @EndpointsAliasProperty(setter=CategoryIdSet) #, required=True)
    def category_id(self):
        if self.category is not None:
            return self.category.string_id()

    def region_by_id(self, region_id):
        """
        Called by RegionIdsSet
        """
        # Verify the region exists
        region = Region.get_by_id(region_id)
        if not region:
            raise endpoints.NotFoundException(
                    "No region found with ID '%s'" % region_id)
        return region

    ################# regions ######################
    def RegionIdsSet(self, region_ids):
        if not isinstance(region_ids, list):
            raise TypeError('region_ids must be a list')
        self.regions = []   # Just delete existing keys?
        for region_id in region_ids:
            region = self.region_by_id(region_id)
            self.regions.append(region.key)

    # Any way to set the return type (as in, to list?)
    @EndpointsAliasProperty(setter=RegionIdsSet, repeated=True) #, required=True)
    def region_ids(self):
        if self.regions is not None:
            return [region_key.id() for region_key in self.regions]


class Phone(EndpointsModel):
    """
    Phone number

    A number consists of area, prefix and line
    """
    pass


class Post(EndpointsModel):
    """
    A Craigslist post
    """

    DEFAULT_ORDER = '-published,'

    # Default fields in message when not overridden by method decorator
    MESSAGE_FIELDS = ('id', 'parent', 'title', 'description', 'img_link',
                     'link', 'location', 'photo', 'source', 'email', 'phone',
                     'str_published', 'str_updated')

    _message_fields_schema = MESSAGE_FIELDS

    _id = None
    _parent = None

    created     = ndb.DateTimeProperty(auto_now_add=True)
    description = ndb.  StringProperty()
    email       = ndb.  StringProperty()
    link        = ndb.  StringProperty()
    location    = ndb.   GeoPtProperty()
    owner       = ndb.    UserProperty()
    img_link    = ndb.  StringProperty()
    photo       = ndb.    BlobProperty()
    published   = ndb.DateTimeProperty()
    phone       = ndb.  StringProperty()  # link or 
    source      = ndb.  PickleProperty(compressed=True)        # serialized dict
    title       = ndb.  StringProperty()
    updated     = ndb.DateTimeProperty()

    # Ancestors stuff from EPD tutorial
    # Leaving here for reference
    #
    # Here's how to get the ID of child entity
#   feed_id = 'feed1'
#   codelab_id = 'codelab1'
#   feed_key = ndb.Key(Feed, feed_id)
#   codelab = Codelab.get_by_id(codelab_id, feed_key)

    def set_key(self):
        if self._parent is not None and self._id is not None:
            key = ndb.Key(Feed, self._parent, Post, self._id)
            self.UpdateFromKey(key)

    def set_parts(self):
        if self.key is not None:
            parent_pair, id_pair = self.key.pairs()
            self._parent = parent_pair[1]
            self._id = id_pair[1]

    def set_parent(self, value):
        if not isinstance(value, basestring):
            raise TypeError('Feed name must be a string')
        self._parent = value
        if ndb.Key(Feed, value).get() is None:
            raise endpoints.NotFoundException('Feed %s does not exist' % value)
        self.set_key()
        self._endpoints_query_info.ancestor = ndb.Key(Feed, value)

    @EndpointsAliasProperty(setter = set_parent, required = True)
    def parent(self):
        if self._parent is None:
            self.set_parts()
        return self._parent

    def IdSet(self, value):
        if not isinstance(value, basestring):
            raise TypeError('ID must be a string')
        self._id = value
        self.set_key()

    @EndpointsAliasProperty(setter=IdSet, required=True)
    def id(self):
        if self.key is not None:
            return self.key.string_id()

    # Set published value to/from string
    # Included here mainly for reference
    def PublishedSet(self, value):
        self.published = strptime(value)

    # Print date as string
    @EndpointsAliasProperty(setter=PublishedSet)
    def str_published(self):
        return strftime(self.published)

    def UpdatedSet(self, value):
        self.updated = strptime(value)

    @EndpointsAliasProperty(setter=UpdatedSet)
    def str_updated(self):
        return strftime(self.updated)
