from protorpc import                        remote
import                                      service
from models import                          Post as Model
from pdb import                             set_trace
from google.appengine.api.mail import       EmailMessage
from models import                          Feed
import                                      re
from settings import                        settings


# Query body must contain parent
QUERY_FIELDS = ('limit', 'order', 'pageToken', 'parent', 'email', 'phone')

# Unlike query, body for insert need not contain parent
CREATE_FIELDS = tuple(set(Model.MESSAGE_FIELDS) - set(['id', 'parent']))

# fields for update, patch methods
UPDATE_FIELDS = ('title', 'description', 'photo', 'email', 'phone')

USER_REQUIRED = settings.user_required

#EMAIL_SENDER = 'tmst08@gmail.com'
EMAIL_SENDER = 'Craigslist Search Notifier <noreply@craigslist-epd.appspotmail.com>'

@service.api_root.api_class(resource_name='posts', path='posts')
class PostsService(remote.Service):
    """
    The Model API methods
    """
    def do_notify(self, my_model):
        """
        Send an email to recipient containing the items in the given feed.

        recipient: owner of the search
        search_title: Subject of email
        posts: items inserted into datastore
        feed: URL for "all results" link in notification

        Currently can handle only email notifications
        """

        # Isn't `Post.parent` supposed to be a key instance?
        feed = Feed.get_by_id(my_model.parent)

        # TODO: Verify notification method (text/email)
        if not feed.notification:
            return

        # Notification sent to search owner
        recipient = feed.owner.email()

        # TODO: Figure out photo encoding. Are photos really
        #       PNGs? If so, why correct display in browser?
        # TODO: Quote text (or htmlescape) in HTML attrs (mailto subject, etc)
        # TODO: Remove "[&]format=rss" from "All results" link


        # TODO: This is problematic in emails. Is
        # there an encoding option I should know about?
        link = my_model.img_link
        if link == None:
            # Handle "AttributeError: 'NoneType' object has
            #  no attribute 'encode'"
            link = ""
        else:
            link = link.encode('ascii', 'ignore')

        template = """\
            <html>
                <head>
                    <style>
                        p &#123; 
                            font-family: sans-serif;
                            margin-top: 10px;
                            margin-bottom: 10px;
                            margin-right: 15px;
                            margin-left: 8px;
                        &#125;
                    </style>
                    <title>{1}</title>
                </head>
                <body>
                    <!-- No worky
                    <img src="data:image/jpg;base64, {7}">
                    -->
                    <p><img src="{6}"></p>
                    <div>
                        <h3>{1}</h3>
                        <p>{8}</p>
                        <p>Photo: <a href="{6}">{6}</a></p>
                        <p>Phone: <a href="{2}">{2}</a></p>
                        <p>Email: <a href="mailto: {3}?
                                     subject={0}&
                                     body={1}">
                            {3}
                        </a></p>
                        <p>Original listing: <a href="{4}">{4}</a></p>
                        <p>Posted: {5}</p>
                    </div>
                </body>
            </html>
        """

        try:
            html = template.format(
                    feed.title,             # TODO: HTML escape?
                    my_model.title,
                    my_model.phone or "",   # or "email only"?
                    my_model.email or "",
                    my_model.link,
                    my_model.str_published,
                    my_model.img_link,
                    my_model.photo,
                    my_model.description    # TODO: HTML escape?
            )
        except UnicodeEncodeError:
            # img_link has a wierd character
            # 'ascii' codec can't encode character u'\xae' in
            # position 95: ordinal not in range(128)

            html = "Error formatting message: %s %s" % (
                my_model.title, my_model.link)

        # Compose, send notification
        #
        message = EmailMessage()
        # TODO: get sender from app.yaml or something
        message.sender = EMAIL_SENDER
        message.to = [recipient]
        message.subject = "%s - %s" % (feed.title, my_model.title)
        message.html = html
        message.to_mime_message()   # unnec?
        message.check_initialized()
        message.send()


    # insert
    @Model.method(http_method = 'PUT',
                  request_fields = CREATE_FIELDS,
                  path = '{parent}/{id}',
                  user_required = USER_REQUIRED)
    def insert(self, my_model):
        # NOTE: If auth req'd and no user, then Unauthored has already been
        # thrown by the service module and this code never executes.
        # TODO: Why 503 back end error instead of 4?? when no ID?
#       service.check_permissions(my_model, 'GET')

        self.do_notify(my_model)
        return service.insert(my_model)

    # get
    @Model.method(http_method = 'GET',
                  # Unlike the `list` response, this
                  # contains all fields
                  path = '{parent}/{id}',
                  user_required = USER_REQUIRED)
    def get(self, my_model):
        return service.get(my_model)

    # update
    @Model.method(http_method = 'POST',
                  path = '{parent}/{id}',
                  request_fields = UPDATE_FIELDS,
                  user_required = USER_REQUIRED)
    def update(self, my_model):
        return service.update(my_model)

    # delete
    @Model.method(http_method = 'DELETE',
                 path = '{parent}/{id}',
                 response_fields = ('id', 'title'),
                 user_required = USER_REQUIRED)
    def delete(self, my_model):
        return service.delete(my_model)

    # list
    @Model.query_method(query_fields = QUERY_FIELDS,
                        path = '{parent}',
                        user_required = USER_REQUIRED)
    def list  (self, query):
        return service.list(query)
