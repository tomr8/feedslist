import endpoints
import logging
import models
import pdb
from google.appengine.ext import ndb
from settings import PERMISSION
from settings import ROLE
from settings import get_permissions
from settings import is_system_kind
from settings import get_user_role
from settings import settings

# api_root used to decorate API classes in the service modules
#
api_root = endpoints.api(
                      name = settings.api.name,
                   version = settings.api.version,
               description = settings.api.description,
        allowed_client_ids = settings.allowed_client_ids,
                auth_level = settings.auth_level)

def authorize(permission, kind=None, entity=None):
    """
    Throw an exception if there is a problem with the request. Used in
    methods in which the object already exists (GET, UPDATE, etc).

    One of either kind or entity is required
    """
    kind = kind or entity.key.kind()
    user = endpoints.get_current_user()
    role = get_user_role(user)

    perms = get_permissions(kind, role)
    if permission not in perms:
        # The user's role doesn't have this permission on this kind
        person = "User %s" % user if user else "Unauthenticated user"
        raise endpoints.UnauthorizedException('%s not authorized' % person)

    # Verify user owns the entity being viewed, updated or deleted
    # Note: `list` not checked here (as no entity)
    if not is_system_kind(kind):
        # These may be seen only by owner
        if entity and (user != entity.owner):   # and (role != ROLE.ADMIN)?
            # Deny attempt to manage entity owned by another. Deny.
            raise endpoints.ForbiddenException('Resource owned by another user.')

def insert(my_model):
    """
    Insert an entity in the datastore

    Despite having a valid entity (my_model), `authorize` called with
    entity kind rather than entity to prevent the owner check from
    being performed there. Obviously a new entity doesn't need to be
    verified as owned by the current user.
    """
    kind = my_model.key.kind()
    authorize(PERMISSION.CREATE, kind=kind)
    # Verify model not already in datastore
    if my_model.from_datastore:
        name = my_model.key.string_id()
        raise endpoints.BadRequestException(
                'A `%s` with the ID \'%s\' already exists.' % (kind, name,))
    my_model.owner = endpoints.get_current_user()
    my_model.put()
    return my_model

def get(my_model):
    """
    Retrieve an entity from the datastore
    """
    if not my_model.from_datastore:
        raise endpoints.NotFoundException('Requested entity was not found.')
    authorize(PERMISSION.READ, entity=my_model)
    return my_model

def update(my_model):
    """
    Replace all attribute values of a datastore entity
    """
    if not my_model.from_datastore:
        raise endpoints.NotFoundException('Requested entity was not found.')
    authorize(PERMISSION.UPDATE, entity=my_model)
    # admin update should not change owner
    # TODO: put into tests
#   my_model.owner = endpoints.get_current_user() # update vs patch
    my_model.put()
    return my_model

def patch(my_model):
    """
    Replace a subset of attribute values of a datastore entity
    """
    if not my_model.from_datastore:
        raise endpoints.NotFoundException('Requested entity was not found.')
    authorize(PERMISSION.UPDATE, entity=my_model)
    # owner is NOT updated
    my_model.put()
    return my_model

def delete(my_model):
    """
    Delete an entity and its children from the datastore
    """
    if not my_model.from_datastore:
        raise endpoints.NotFoundException('Requested entity was not found.')
    authorize(PERMISSION.DELETE, entity=my_model)
    # TODO: Add recursion to delete entire child tree 
    # No Parent kind needed - woo hoo!
    children = ndb.Query(ancestor = my_model.key).fetch()
    # Delete children of my_model
    for child in children:
        # Parent entity (my_model) returned in list of children
        #
        # No need to check permissions, because a child will
        # always be owned by the owner of its ancestor(s)
#       authorize(PERMISSION.DELETE, entity=child)
        child.key.delete()
    return my_model

def list(query):
    """
    List entities owned by the current user
    """
    # NOTE: Datastore docs: "It's possible to augment
    # the query using environment variables and other
    # parts of the request state."
    kind = query.kind
    authorize(PERMISSION.READ, kind=kind)
    if is_system_kind(kind):
        # This is a "system" entity
        filter = lambda query: query.filter()
    else:
        user = endpoints.get_current_user()
        Kind = eval('models.%s' % kind)
        filter = lambda query: query.filter(Kind.owner == user)
    return filter(query)
