import os
from bunch import bunchify
import yaml
import endpoints
import pdb


SETTINGS_YAML = 'settings.yaml'

def _Enum(docstring, *names):
    """Utility to generate enum classes used by annotations.

    Args:
    docstring: Docstring for the generated enum class.
    *names: Enum names.

    Returns:
    A class that contains enum names as attributes.
    """
    enums = dict(zip(names, range(len(names))))
    reverse = dict((value, key) for key, value in enums.iteritems())
    enums['reverse_mapping'] = reverse
    enums['__doc__'] = docstring
    return type('Enum', (object,), enums)

_ROLE_DOCSTRING = """
    Define the role enums used when authorizing actions by users

    Valid values of role and their meanings are:

    ROLE.NONE:

    ROLE.ADMIN:

    ROLE.USER:
    """

ROLE = _Enum(_ROLE_DOCSTRING, 'NONE', 'ADMIN', 'USER')

_PERMISSION_DOCSTRING = """
    Define the permission enums used when authorizing actions by users

    Valid values of permission and their meanings are:

    PERMISSION.CREATE:

    PERMISSION.READ:

    PERMISSION.UPDATE:

    PERMISSION.DELETE:
    """

PERMISSION = _Enum(_PERMISSION_DOCSTRING, 'CREATE', 'READ', 'UPDATE', 'DELETE')

def recursive_replace(container, old, new):
    """
    Walk the dictionary `container`. If a key has a value
    matching `old`, replace the value with `new`. If the
    container has a key matching `old`, create a `new`
    key with the name `new`, assign the `old` value to
    it and delete `old`.
    """
    if isinstance(container, dict):
        for key, value in container.iteritems():
            if key == old:
                # NOTE: this is alpha code
                container[new] = value
                del container[key]
            if value == old:
                container[key] = new
            if isinstance(value, (dict, list)):
                recursive_replace(value, old, new)
    else:
        for ix, value in enumerate(container):
            if value == old:
                container[ix] = new

def get_permissions(kind, role):
    """
    Returns the permissions granted to the
    given role for the specified kind of entity.

    If settings doesn't specify roles for the given
    kind, or permissions for the given role, return
    the empty list.

    Input:
        kind: entity kind: string
        role: user role: enum (int)
    Output:
        permissions: allowed operations: List<int>
    """
    # Get the roles specified for this kind
    roles = settings.permissions.get(kind, {})
    # return allowed operations for the role
    return roles.get(role, [])

def is_system_kind(kind):
    """
    Return True if admin permitted to create instances
    """
    perms = get_permissions(kind, ROLE.ADMIN)
    return PERMISSION.CREATE in perms

def get_user_role(user):
    """ Get the role for the given user

        No need to call get_current_user() here as
        it is usually already being called in caller
    """
    user_email = user.email() if user else None
    return settings.roles.get(user_email, None)

with open(os.path.join(os.getcwd(), SETTINGS_YAML)) as f:
    settings = yaml.load(f)

# Replace constants
recursive_replace(settings, 'API_EXPLORER_CLIENT_ID',
                  endpoints.API_EXPLORER_CLIENT_ID)

for const in ['PERMISSION.CREATE',
              'PERMISSION.READ',
              'PERMISSION.UPDATE',
              'PERMISSION.DELETE',
              'ROLE.NONE',
              'ROLE.ADMIN',
              'ROLE.USER']:
    recursive_replace(settings, const, eval(const))

recursive_replace(settings, 'USERINFO',
                  'https://www.googleapis.com/auth/userinfo.email')

auth_level = settings.get('auth_level', 'optional')

settings['auth_level'] = {
             'required': endpoints.AUTH_LEVEL.REQUIRED,
             'optional': endpoints.AUTH_LEVEL.OPTIONAL,
    'optional_continue': endpoints.AUTH_LEVEL.OPTIONAL_CONTINUE,
                 'none': endpoints.AUTH_LEVEL.NONE
}.get(auth_level)

# export the settings
settings = bunchify(settings)
