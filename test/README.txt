See bottom for 'historical', etc

back end
  security
    API visible only to authenticated user?
    API visible only to authorized user?
    Discovery doc visibility (authenticated/authorized)
    Levels of security
      endpoint, API, service (class), method
    HTTP methods
      CRUD
        separate auth by groups
          list, get
          insert
          patch, update, delete
    authorization
      anonymous user presented with login screen?
    authentication

    Fuzzing attacks
      Set up assertions that validate the response
        consistent and concise
        use the correct HTTP error code
        do not expose any sensitive system information

    Invalid Input Attacks
      string where integer expected, and vice-versa
        (get codelabs/abc)
      input missing/too long
      verify input headers and values
      Automate boundary/invalid-input tests
      Configure assertions to validate error messages

    Injection attacks
      Look at Datastore code in Model.py to determine how requests are parsed
       and queries constructed. Determine how an injection might be performed,
       and how it might be detected or ignored. Verify appropriate error
       response (404, 400, etc) is returned.
      Block access to a user who submits a sequence of arbitrary requests.

Learn Python unittest (std lib)
Learn GAE testbed
Learn webtest (Pylons project)
Check out Ferris nose

check out Google's docs on implementing a multi-class API:
https://developers.google.com/appengine/docs/python/endpoints/create_api#creating_an_api_implemented_with_multiple_classes
7/6/17 (Wed)
--------------
Perform tests using runner.py:
$ ./runner.py ~/work/google-cloud-sdk

Runs test modules with naming pattern "*_test.py"

Currently, only functional tests in api_test.py work. Integration tests in test_server.py fail due to not being able to find the module `craiglist`Setup:


Historical:

  Execute the following to run the test suite:
  $> python runner.py /home/tom/work/google-cloud-sdk ~/work/Playground/python/endpoints-proto-datastore/tests

  runner.py
    Loads the tests that exist in the package specified on the command line
  test_api.py
    Contains the tests loaded by runner.py
    Lives in the package specified on the runner.py command line
  codelabs.py
    Contains the service (API)
    Imported by test_api.py

Status:
  Currently returning 404:
  "AppError: Bad response: 404 Not Found (not 200 OK or 3xx redirect for http://localhost/_ah/spi/codelabs.list)
  Not Found"

  What does this mean?:
    The request is not reaching the server?
    The `insert` method is not found? (most likely)
    The response from "insert" is Not Found?

  Most likely problem is an incorrect service path in test_api.py:
      resp = testapp.post_json('/_ah/spi/codelabs.list')

Update:
  Changed service path above to:
      resp = testapp.post_json('/_ah/spi/CodelabsService.list')
  Got a 401: Unauthorized
    Woo hoo!
  Turned off auth in codelabs.py
  Now getting 
AppError: Bad response: 500 Internal Server Error (not 200 OK or 3xx redirect for http://localhost/_ah/spi/CodelabsService.list)
'{"state": "SERVER_ERROR", "error_message": "Internal Server Error"}

Next:
  Since the current test_api.py code was lifted from a possibly-outdated and 
   abbreviated example on S/O, the next thing to try is to set up according to
   the "Local Unit Testing for Python" example:
    https://cloud.google.com/appengine/docs/python/tools/localunittesting

  Update:
    Figure out how to authenticate in the test runner
    Or, better, first how to run a test without auth

  Update:
    modify `list` method body
