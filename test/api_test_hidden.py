"""
Taken directly from S/O (1st example) at
<SO questions>?/20384743/how-to-unit-test-google-cloud-endpoints

Largely matches example at
https://cloud.google.com/appengine/docs/python/tools/localunittesting
"""

"""
Importing endpoints from google.appengine.ext is deprecated and will be removed.  Add the endpoints library to app.yaml, then endpoints can be imported simply with "import endpoints.
from google.appengine.ext import endpoints
"""

# Add the project directory to module search path
import sys
from copy import deepcopy
import endpoints  # ensure PYTHONPATH includes 'endpoints-1.0'
from google.appengine.ext import testbed
from google.appengine.ext import ndb
import webtest
import unittest
import os
import pdb # Note: set_trace() can cause error msgs to have incorrect line numbers

sys.path.append('..')
import main
import models
from kinds import Kinds

# These must match entries in ADMIN_USERS
#  defined in the service module
# TODO: How does that work?
ADMIN_USER = 'tmst08@gmail.com'
OWNER_USER = 'tomr@pylomer.com'  # creates the test object(s)
OTHER_USER = 'tomr63@gmail.com'

class ExtendedTestCase(unittest.TestCase):
    """ Found this on S/O, hoping to find out why my error
        message is not being printed from assertRaisesRegexp.
    """

    def assertRaisesWithMessage(self, msg, func, *args, **kwargs):
        """ Find out why error doesn't get printed from
            testcase.assertRaisesRegExp

            It doesn't look like it would work. And it doesn't.
        """
        try:
            func(*args, **kwargs)
            self.assertFail()
        except Exception as inst:
            self.assertEqual(inst.message, msg)

#class TestEndpointMethods(ExtendedTestCase):
class TestEndpointMethods(unittest.TestCase):
    """ Test endpoints methods """

    def setUp(self):
        tb = testbed.Testbed()
        # Needed because endpoints expects a . in this value
        tb.setup_env(current_version_id='testbed.version')
        tb.activate()
#       tb.init_all_stubs()
        tb.init_datastore_v3_stub()
        tb.init_memcache_stub()
        tb.init_mail_stub()
        self.testbed = tb

        ndb.get_context().clear_cache()

        # NOTE: It's a good idea to create this anew
        # for each test or set of tests
        self.app = webtest.TestApp(main.application)

    def tearDown(self):
        self.testbed.deactivate()

    def login(self, user):
        """
        Simulate authentication. Works with testbed.
        From ferrisnose.utilities
        `email`: string (any)
        `admin`: bool (has no effect, apparently)
        """
        # NOTE: `login_user` from ferrisnose.testbed:
#       os.environ['USER_EMAIL'] = email
#       os.environ['USER_ID'] = email
#       os.environ['USER_IS_ADMIN'] ='1' if admin else '0'
#       os.environ['AUTH_DOMAIN'] = 'gmail.com'

        admin = True if user == ADMIN_USER else False

        os.environ['ENDPOINTS_AUTH_EMAIL'] = user
        os.environ['ENDPOINTS_AUTH_DOMAIN'] = 'gmail'
        os.environ['USER_IS_ADMIN'] = '1' if admin else '0'

    def logout(self):
        """
        Simulate de-authentication
        """
        # TODO: Make this a test?
        #       What happens when a non-test method fails an assertion?
        for var in ['ENDPOINTS_AUTH_EMAIL',
                    'ENDPOINTS_AUTH_DOMAIN',
                    'USER_IS_ADMIN']:
            try:
                del os.environ[var]
            except KeyError:
                # `var` doesn't exist
                pass

    def insert(self):
        """ Not a test method so can use for setups
        """

        url = '/_ah/api/%s.insert'

        # Admin user to insert category
        self.login(ADMIN_USER)

        # insert category
        pdb.set_trace()
        resp = self.app.post_json(url % 'CategoriesService', Kinds.category)
        self.assertEqual(resp.status_code, 200)
        self.assertDictContainsSubset({'id': 'testcat'}, resp.json, "ID mismatch")

        # insert region
        resp = self.app.post_json(url % 'RegionsService', Kinds.region)
        self.assertEqual(resp.status_code, 200)
        self.assertDictContainsSubset({'id': 'testreg'}, resp.json, "ID mismatch")

        self.logout()

        # Normal user to insert feed, etc
        self.login(OWNER_USER)

        # insert feed
        resp = self.app.post_json(url % 'FeedsService', Kinds.feed)
        self.assertEqual(resp.status_code, 200)
        self.assertDictContainsSubset({'id': 'testfeed'}, resp.json, "ID mismatch")

        # insert post
        resp = self.app.post_json(url % 'PostsService', Kinds.post)
        self.assertEqual(resp.status_code, 200)
        self.assertDictContainsSubset({'id': 'testpost'}, resp.json, "ID mismatch")

        # TODO: test deleting entities
        # TODO: test deleting feed having posts

        self.logout()

    def execute(self, request, eid):
        """ Execute the specified request as different users
            Params:
            url: "Posts.list" with possible parent and body
            eid: The entity ID to use for get, etc
            request: ?
        """
        # TODO: rename auth_test

        # Populate the db
        self.insert()

        # owner - 1 item returned
        self.login(OWNER_USER)
        resp = request()
        items = resp.json['items']
        self.assertEqual(1, len(items))
        self.logout()

        # unauthenticated - unauthorized
        with self.assertRaisesRegexp(webtest.AppError, '401 Unauthorized'):
            self.logout()
            request()

        # not owner - no items returned
        with self.assertRaises(KeyError):
            # Item not visible to anonymous
            self.login(OTHER_USER)
            resp = request()
            items = resp.json['items']
            ids = [item['id'] for item in items]
            if 'testreg' in ids or 'testcat' in ids:
                # admin object visble to everyone. Pass test
                raise KeyError
            self.logout()

        # not owner - no items returned
        with self.assertRaises(KeyError):
            # Item not visible to anonymous
            self.login(ADMIN_USER)
            resp = request()
            items = resp.json['items']
            ids = [item['id'] for item in items]
            if 'testreg' in ids or 'testcat' in ids:
                # admin object visble to everyone. Pass test
                raise KeyError
            self.logout()

    def test_inserts(self):
        """ Insert an entity of each Kind
        """
        self.insert()

    def test_region_list(self):
        """ Test `list` method auth.
        """

        url = '/_ah/spi/RegionsService.list'
        body = {}
        request = lambda: self.app.post_json(url, body)

        self.execute(request, eid=None)

    def test_feed_list(self):
        """ Test `list` method auth.
        """

        url = '/_ah/spi/FeedsService.list'
        body = {}
        request = lambda: self.app.post_json(url, body)

        self.execute(request, eid=None)

    def test_post_list(self):
        """ Test `list` method auth.
        """

        eid = Kinds.post['id']
        url = '/_ah/spi/PostsService.list'
        parent = Kinds.post['parent']
        body = {'parent': parent}
        request = lambda: self.app.post_json(url, body)

        self.execute(request, eid)
