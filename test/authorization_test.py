import unittest
from settings import PERMISSION
from settings import ROLE
from settings import get_permissions

class AuthorizationTestCase(unittest.TestCase):
    """ Note: No setup/teardown needed
    """

    def test_admin_may_create_category(self):
        """
        Admin user may create category
        """
        self.assertTrue(
                PERMISSION.CREATE in get_permissions('Category', ROLE.ADMIN))

    def test_user_may_not_create_category(self):
        """
        Normal user may not create category
        """
        self.assertFalse(
                PERMISSION.CREATE in get_permissions('Category', ROLE.USER))

    def test_admin_may_not_create_feed(self):
        """
        Admin user may not create feed
        """
        self.assertFalse(
                PERMISSION.CREATE in get_permissions('Feed', ROLE.ADMIN))

    def test_user_may_create_feed(self):
        """
        Normal user may create feed
        """
        self.assertTrue(
                PERMISSION.CREATE in get_permissions('Feed', ROLE.USER))
