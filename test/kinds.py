# Global defs
class Kinds:
    """ Enable 'dot' access to test fixtures
    """
    category = {       u"id": u"testcat",
                    u"title": u"Category Title",
              u"description": u"Category Description"
    }

    region   = {
                       u"id": u"testreg",
                u"subdomain": u"subdomain1",
           u"subdomain_only": False,
                u"subdomain": u"subdomain2",
           u"subdomain_only": False
    }

    feed     = {
                       u"id": u"testfeed",
                    u"title": u"Test Feed",
              u"description": u"A test feed",
              u"category_id": u"testcat",
               u"region_ids": [u"testreg"],
           u"interval_fetch": False
    }

    post     = {
                       u"id": u"testpost",
                    u"title": u"Test Post",
              u"description": u"A test post",
                   u"parent": u"testfeed"
    }
