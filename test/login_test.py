import unittest

from google.appengine.api import users
from google.appengine.ext import testbed

ADMIN_EMAIL = 'tmst08@gmail.com'
USER_EMAIL = 'tomr@pylomer.com'

class LoginTestCase(unittest.TestCase):
    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_user_stub()

    def tearDown(self):
        self.testbed.deactivate()

    def login_user(self, email='tmst08@gmail.com', id='123', is_admin=False):
        """ Not really valid as back end doesn't use
           `is_current_user_admin`
        """
        self.testbed.setup_env(
            user_email=email,
            user_id=id,
            user_is_admin='1' if is_admin else '0',
            overwrite=True)

    def test_admin_login(self):
        """Login with admin email works
        """
        self.login_user(ADMIN_EMAIL, is_admin=True)
        self.assertEquals(users.get_current_user().email(), ADMIN_EMAIL)
        self.assertTrue(users.is_current_user_admin())

    def test_user_login(self):
        """Login with user email works
        """
        self.login_user(USER_EMAIL, is_admin=False)
        self.assertEquals(users.get_current_user().email(), USER_EMAIL)
        self.assertFalse(users.is_current_user_admin())
