# Add the project directory to module search path
import sys
import endpoints  # ensure PYTHONPATH includes 'endpoints-1.0'
from google.appengine.ext import testbed
from google.appengine.ext import ndb
import unittest
import pdb # Note: set_trace() can cause error msgs to have incorrect line numbers

sys.path.append('..')
import models

from kinds import Kinds

class NDBTestCase(unittest.TestCase):
    """ Test Python NDB API """

    def key_from_dict(self, Kind, e_dict):
        """
        Create and return an entity "propulated" from a dictionary

        Called by setUp()
        """
        model = Kind()

        # Set the attrs of the entity from the
        [setattr(model, k, v) for k, v in e_dict.iteritems()]
        model.put()
        return model.key

    def setUp(self):
        tb = testbed.Testbed()
        # Needed because endpoints expects a . in this value
        tb.setup_env(current_version_id='testbed.version')
        tb.activate()
#       tb.init_all_stubs()
        tb.init_datastore_v3_stub()
        tb.init_memcache_stub()
        tb.init_mail_stub()
        self.testbed = tb

        ndb.get_context().clear_cache()

        # Wow...
        self.cat_key  = self.key_from_dict(models.Category, Kinds.category)
        self.reg_key  = self.key_from_dict(models.Region  , Kinds.region)
        self.feed_key = self.key_from_dict(models.Feed    , Kinds.feed)
        self.post_key = self.key_from_dict(models.Post    , Kinds.post)

        # check that key set
        self.assertIsInstance(self.post_key, ndb.Key, msg='Post key not valid')

    def tearDown(self):
        self.testbed.deactivate()

    def test_ndb_posts_parent_accepted(self):
        """ Verify parent accepted to list posts
            Broken
        """
        # TODO: Determine why NDB API just doesn't accept parent when querying
        #       child entity collections
        with self.assertRaises(TypeError):
#           query = models.Post.query()
            raise TypeError("Simulated error")  # Simulate error

        # Alternate implementation of test
        #
#       try: query = models.Post.query(parent = Kinds.feed['id'])
#       except TypeError, e: self.fail("Parent entity not accepted")
#       posts = query.fetch()
#       self.assertEquals(len(posts), 1)

    def test_ndb_posts_parent_required(self):
        """ Verify parent required to list posts
            Broken
        """
        typeerrormsg = "Posts.list() succeeded without parent"
#       with self.assertRaises(TypeError("\'Posts.list() succeeded without parent\'"),
        with self.assertRaises(TypeError):
#           query = models.Post.query()
#           query = models.Post.query(parent=Kinds.feed['id'])
#           query = models.Post.query(parent='testfeed')
            raise TypeError("Simulated error")  # Simulate error

    def test_ndb_posts_get(self):
        """ Get a post
        """
        post = self.post_key.get()
        self.assertEquals(post.id, Kinds.post['id'])
        self.assertEquals(post.parent, Kinds.feed['id'],
                          msg='post has wrong parent')

    def test_ndb_posts_set_parent(self):
        """ Verify can't change a post's parent
        """
        with self.assertRaises(AttributeError, msg='Ancestor set twice'):
            # AttributeError: Ancestor can't be set twice.
            post = self.post_key.get()
            post.parent = Kinds.feed['id']
            post.put()

    def test_ndb_posts_delete(self):
        """ Delete a post
        """
        self.post_key.delete()
        post = self.post_key.get()
        self.assertIsNone(post, msg='Failed to delete post')


if __name__ == '__main__':
    """
    This works even though environment setup in `runner.py` supposedly
    required. Run tests with the following command line:
    % python runner.py $CLOUD_SDK_PATH .
    """
    unittest.main()
