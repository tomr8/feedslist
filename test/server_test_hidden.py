#!/usr/bin/env python2

import sys
import os
import dev_appserver
sys.path.append('/home/tom/work/feedslist')
#sys.path[1:1] = dev_appserver._DEVAPPSERVER2_PATHS
dev_appserver.fix_sys_path()    # TODO: add comment on S/O

import pdb
#pdb.set_trace()

# Import the application module (main.py)
sys.path.insert(0, '/home/tom/work/feedslist')

from google.appengine.tools.devappserver2 import devappserver2
from google.appengine.tools.devappserver2 import python_runtime

sys.path.insert(0, '/home/tom/work/google-cloud-sdk/platform/google_appengine/lib/apiclient')

# No longer needed
#sys.path.insert(0, '/home/tom/work/google-cloud-sdk/platform/google_appengine/lib/webapp2-2.5.2')
#sys.path.insert(0, '/home/tom/work/google-cloud-sdk/platform/google_appengine/lib')

import apiclient
import apiclient.discovery

import unittest
import httplib2
import pdb
import argparse

#sys.path.insert(0, '/home/tom/work/google-cloud-sdk/platform/google_appengine/lib/oauth2client')
import oauth2client  # BEFORE file, tools!
import oauth2client.file  # wierd import required
import subprocess
import time

YAML_PATH = 'app.yaml'    # NOTE: run inside tests/
#API_ROOT = 'https://epd-example-basic.appspot.com/_ah/api'
API_ROOT = 'http://localhost:8123/_ah/api'
API_PORT = '8123' # must be string
API      = 'rest'
VERSION  = 'v1'

# OAuth2 stuff
CLIENT_ID           = '1043357714103-4h38o2e4i6iqtpgcos5q0pg313mq67bh.apps.googleusercontent.com'
CLIENT_SECRET       = 'xQ4rGk0_UlwQzk8UjidNiybv'
CREDENTIALS_FILE    = '../credentials.user.json'
SCOPE               = 'https://www.googleapis.com/auth/userinfo.email'
USER_AGENT          = 'server_test/1.0'
OAUTH_DISPLAY_NAME  = 'My EPD Command-line Tool'
USER_ACCT           = 'tmst08@gmail.com'
NONE_ACCT           = 'none'

def set_account(account):
    """
    Set an account as active. The `gcloud` tool maintains, in the local
    environment, a list of user accounts for which credentials have been
    obtained. Only one account can be active. See `gcloud auth list`.
    """
    # NOTE: any effect on dev_server is unknown.
    # NOTE: May cause problem with deployment.
    cmd = ['gcloud', 'config', 'set', 'account', account]
    subprocess.call(cmd)

class TestApiServer(unittest.TestCase):
    def _get_credentials(self):
        """Gets Google API credentials or generates new ones
           if they don't exist or are invalid.
        """

        storage = oauth2client.file.Storage(CREDENTIALS_FILE)
        credentials = storage.get()

        # WebServerFlow
        #
        if credentials is None or credentials.invalid:
            # NOTE: Fails.
            # TODO: Find out why this fails whereas the same code
            #       in api_client.py succeeds
            parser = argparse.ArgumentParser(
                description='Auth Sample',
                formatter_class=argparse.RawDescriptionHelpFormatter,
                parents=[oauth2client.tools.argparser])
            flags = parser.parse_args(sys.argv[1:])

            flow = oauth2client.client.OAuth2WebServerFlow(
                client_id=CLIENT_ID,
                client_secret=CLIENT_SECRET,
                scope=SCOPE,
                user_agent=USER_AGENT,
                oauth_displayname=OAUTH_DISPLAY_NAME)
            credentials = oauth2client.tools.run_flow(flow, storage, flags)

            storage.put(credentials)

        return credentials

    def setUp(self):
#        APP_CONFIGS = ['/path/to/app.yaml']
        APP_CONFIGS = [YAML_PATH, ]
        python_runtime._RUNTIME_ARGS = [
            sys.executable,
            os.path.join(os.path.dirname(dev_appserver.__file__),
                         '_python_runtime.py')
        ]
        options = devappserver2.PARSER.parse_args([
            '--admin_port', '0',
            '--port', API_PORT,
            '--datastore_path', ':memory:',
            '--logs_path', ':memory:',
            '--skip_sdk_update_check',
            '--dev_appserver_log_level', 'warning',
            '--',
        ] + APP_CONFIGS)
        server = devappserver2.DevelopmentServer()
        server.start(options)
        self.server = server

    def test_apiserver(self):
        """
        Perform a number of operations and make related assertions.
        """

        discoveryUrl = \
                'http://{module}/_ah/api/discovery/v1/apis/{api}/{version}/rest'.format(
                module = self.server.module_to_address('default'),
                api = API,
                version = VERSION)
#        discoveryUrl = '%s/discovery/v1/apis/%s/%s/rest' % (API_ROOT, API, VERSION)
#        http = httplib2.Http()

        credentials = self._get_credentials()

        http = httplib2.Http()
        http = credentials.authorize(http)

        rest = apiclient.discovery.build(
                API, VERSION,
                discoveryServiceUrl = discoveryUrl,
                # Simulate user logged in and not.
                http = http
        )

        # TODO: Why is this necessary? Is there a better way?
        service = rest.FeedsService()  # Nasty hack?

        item_id = 'testfeed'

        body = {
                   u"id": item_id,
                u"title": item_id.upper(),
          u"description": item_id.upper() + " DESCRIPTION",
                u"level": u"easy"
        }

        # Test `insert`
        # activate user acct
#        set_account(USER_ACCT)
        resp = service.insert(id = item_id, body = body).execute()
        self.assertEqual(resp, body)

        # Test `get`
        # deactivate user acct and look for failure
#        set_account(NONE_ACCT)
        resp = service.get(id = item_id).execute()
        self.assertEqual(resp['title'], body['title'])

        # Test `patch`
        title = item_id.lower()
        resp = service.patch(
                  id = item_id,
                body = {"title": title}
        ).execute()
        # Verify only the included field changed
        self.assertEqual(resp['title'], title)
        self.assertEqual(resp['level'], body['level'])

    def tearDown(self):
        self.server.stop()
