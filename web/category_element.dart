import 'package:polymer/polymer.dart';
import 'dart:html' show Event, Node, CustomEvent;
import 'item_element.dart' show ItemElement;	// TODO: , ClientLib;
import 'clientlib.dart' as ClientLib;
import 'dart:mirrors';


@CustomTag('category-element')
class CategoryElement extends ItemElement {

  final itemType = "Category";

  // TODO: Can access Category as ItemElement.Endpoint.Category?
  final apiType = reflectClass(ClientLib.Category);

  CategoryElement.created() : super.created();

  void updateItem(Event e, var detail, Node sender) {
    super.updateItem(e, detail, sender);
    /// Update Item subclass values here.
  }

  void cancelEditing(Event e, var detail, Node sender) {
    super.cancelEditing(e, detail, sender);
    /// Restore unmodified Item subclass values here.
  }

  void startEditing(Event e, var detail, Node sender) {
    super.startEditing(e, detail, sender);
    /// Cache Item subclass values here.
  }

  void deleteItem(Event e, var detail, Node sender) {
    super.deleteItem(e, detail, sender);
    /// Perform any needed Item subclass operations here.
  }

  /// Copies values from source Category to destination Category.
  void copyItem(source, destination) {
    super.copyItem(source, destination);
    /// Copy Item subclass values here.
  }
}
