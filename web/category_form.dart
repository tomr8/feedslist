import 'package:polymer/polymer.dart';
import 'model.dart' show Category;
import 'dart:html' show CustomEvent, Event, Node;
import 'item_form.dart' show ItemFormElement;

/*
 * The class for creating or updating a category. Performs validation based on
 * a category based on validation rules defined in the model.
 */
@CustomTag('category-form')
class CategoryFormElement extends ItemFormElement {

  CategoryFormElement.created() : super.created() {}

  void validateItem(Event event, Object detail, Node sender) {
    /// Superclass validation dispatches custom 'itemvalidated' event
    /// for which the parent (...-list) class registers a listener.
    /// Perform any needed Item subclass validations here, first.
    if (true) {
      super.validateItem(event, detail, sender);
    }
  }

  void cancelForm(Event event, Object detail, Node sender) {
    /// Superclass clears title and description, dispatches
    /// custom 'formnotneeded' event for parent.
    /// Perform any needed subclass operations here.
    super.cancelForm(event, detail, sender);
  }
}
