import 'package:polymer/polymer.dart';
import 'item.dart' show Item;
import 'item_list.dart' show ItemList;	// , RestApi;
import 'dart:html' show Event, Node;
import 'model.dart' show Category;
import 'clientlib.dart' as ClientLib;
import 'dart:mirrors';

/// Class to represent a collection of Category objects.
@CustomTag('category-list')
class CategoryList extends ItemList {
  final itemType = 'Category';

  final apiType = reflectClass(ClientLib.Category);

  static const ALL = "all";

  /// List of filter values. Includes the levels defined in the model, as well
  /// as a filter to return all categories.
  @observable List<String> filters = toObservable([ALL]);

  /// String that stores the value used to filter categories.
  @observable String filterValue = ALL;

  /// The list of filtered categories.
  @observable List<Category> filteredItems = toObservable([]);

  void resetForm() {
    /// Insert needed initializations here
    super.resetForm();
  }

  void addItem(Event e, var detail, Node sender) {
    /// Insert needed initializations here
    super.addItem(e, detail, sender);
  }

  void deleteItem(Event e, var detail, Node sender) {
    /// Insert needed teardown here
    super.deleteItem(e, detail, sender);
  }

  void copyItem(source, destination) {
    super.copyItem(source, destination);
    /// Copy Item subclass values here.
  }

  /// Calculates the categories to display when using a filter.
  void filter() {
    filteredItems = items;
  }

  /// Refreshes the filtered categories list every time the categories list changes.
  /// TODO: consider moving to base class
  void itemsChanged() {
    filter();
  }

  /// Named constructor. Sets initial value of filtered categories and sets
  /// the new category's level to the default.
  CategoryList.created() : super.created() {
    filteredItems = items;
    // TODO: figure out the defaultLevel thing
//  newItem.regionId = defaultLevel;

    /// Get an authenticated client and call `list()`
    var client = getClient().then((client) {
        api = new ClientLib.FeedslistApi(client);
        resourceApi = api.categories;

        results = "Fetching categories...";
        api.categories.list().then((response) {
            /// TODO: Since duplicated in several places,
            /// put this anon function in library
            if (response.items == null)
              results = "Fetched no posts";
            else {
              for(var respItem in response.items) {
                Item item = new Item(itemType, '', '', '');
                copyItem(respItem, item);
                items.add(item);
              }
              results = "Fetched ${response.items.length} categories";
            }
        });
    });
  }
}
