// This is a generated file (see the discoveryapis_generator project).

library polymer_and_dart.feedslist.v1;

import 'dart:core' as core;
import 'dart:async' as async;
import 'dart:convert' as convert;

import 'package:_discoveryapis_commons/_discoveryapis_commons.dart' as commons;
import 'package:http/http.dart' as http;

export 'package:_discoveryapis_commons/_discoveryapis_commons.dart' show
    ApiRequestError, DetailedApiRequestError;

const core.String USER_AGENT = 'dart-api-client feedslist/v1';

/** Feedslist API */
class FeedslistApi {
  /** View your email address */
  static const UserinfoEmailScope = "https://www.googleapis.com/auth/userinfo.email";

  static const localUrl = "http://localhost:8080/_ah/api/";
  static const remoteUrl = "https://craigslist-epd.appspot.com/_ah/api/";

  final commons.ApiRequester _requester;

  CategoriesResourceApi get categories => new CategoriesResourceApi(_requester);
  FeedsResourceApi get feeds => new FeedsResourceApi(_requester);
  PostsResourceApi get posts => new PostsResourceApi(_requester);
  RegionsResourceApi get regions => new RegionsResourceApi(_requester);

  FeedslistApi(http.Client client, {core.String rootUrl: remoteUrl, core.String servicePath: "feedslist/v1/"}) :
      _requester = new commons.ApiRequester(client, rootUrl, servicePath, USER_AGENT);
}


class CategoriesResourceApi {
  final commons.ApiRequester _requester;

  CategoriesResourceApi(commons.ApiRequester client) : 
      _requester = client;

  /**
   *  Delete a Datastore entity
   *
   * Request parameters:
   *
   * [id] - null
   *
   * Completes with a [CategoryProtoIdTitle].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<CategoryProtoIdTitle> delete(core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'categories/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "DELETE",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new CategoryProtoIdTitle.fromJson(data));
  }

  /**
   *  Get a Datastore entity
   *
   * Request parameters:
   *
   * [id] - null
   *
   * Completes with a [CategoryProtoIdTitleDescription].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<CategoryProtoIdTitleDescription> get(core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'categories/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "GET",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new CategoryProtoIdTitleDescription.fromJson(data));
  }

  /**
   *  Insert a Datastore entity
   *
   * [request] - The metadata request object.
   *
   * Request parameters:
   *
   * [id] - null
   *
   * Completes with a [Category].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<Category> insert(CategoryProtoDescriptionTitle request, core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (request != null) {
      _body = convert.JSON.encode((request).toJson());
    }
    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'categories/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "PUT",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new Category.fromJson(data));
  }

  /**
   *  List Datastore entities
   *
   * Request parameters:
   *
   * [limit] - null
   *
   * [order] - null
   *
   * [pageToken] - null
   *
   * Completes with a [CategoryCollection].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<CategoryCollection> list({core.String limit, core.String order, core.String pageToken}) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (limit != null) {
      _queryParams["limit"] = [limit];
    }
    if (order != null) {
      _queryParams["order"] = [order];
    }
    if (pageToken != null) {
      _queryParams["pageToken"] = [pageToken];
    }

    _url = 'categories/list';

    var _response = _requester.request(_url,
                                       "GET",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new CategoryCollection.fromJson(data));
  }

  /**
   *  Update a Datastore entity
   *
   * [request] - The metadata request object.
   *
   * Request parameters:
   *
   * [id] - null
   *
   * Completes with a [Category].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<Category> update(CategoryProtoDescriptionTitle request, core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (request != null) {
      _body = convert.JSON.encode((request).toJson());
    }
    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'categories/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "POST",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new Category.fromJson(data));
  }

}


class FeedsResourceApi {
  final commons.ApiRequester _requester;

  FeedsResourceApi(commons.ApiRequester client) : 
      _requester = client;

  /**
   * Request parameters:
   *
   * [id] - null
   *
   * Completes with a [FeedProtoIdTitle].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<FeedProtoIdTitle> delete(core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'feeds/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "DELETE",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new FeedProtoIdTitle.fromJson(data));
  }

  /**
   * Request parameters:
   *
   * [id] - null
   *
   * Completes with a
   * [FeedProtoIdTitleDescriptionCategoryIdRegionIdsQueryObjIntervalFetchNotificationStatus].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<FeedProtoIdTitleDescriptionCategoryIdRegionIdsQueryObjIntervalFetchNotificationStatus> get(core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'feeds/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "GET",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new FeedProtoIdTitleDescriptionCategoryIdRegionIdsQueryObjIntervalFetchNotificationStatus.fromJson(data));
  }

  /**
   * [request] - The metadata request object.
   *
   * Request parameters:
   *
   * [id] - null
   *
   * Completes with a
   * [FeedProtoIdTitleDescriptionCategoryIdRegionIdsQueryObjIntervalFetchNotificationStatus].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<FeedProtoIdTitleDescriptionCategoryIdRegionIdsQueryObjIntervalFetchNotificationStatus> insert(FeedProtoRegionIdsCategoryIdQueryObjIntervalFetchNotificationDescriptionTitleStatus request, core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (request != null) {
      _body = convert.JSON.encode((request).toJson());
    }
    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'feeds/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "PUT",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new FeedProtoIdTitleDescriptionCategoryIdRegionIdsQueryObjIntervalFetchNotificationStatus.fromJson(data));
  }

  /**
   * Request parameters:
   *
   * [categoryId] - null
   *
   * [intervalFetch] - null
   *
   * [limit] - null
   *
   * [notification] - null
   * Possible string values are:
   * - "NONE"
   * - "EMAIL"
   * - "TEXT"
   * - "BOTH"
   *
   * [order] - null
   *
   * [pageToken] - null
   *
   * [regionIds] - null
   *
   * Completes with a [FeedCollection].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<FeedCollection> list({core.String categoryId, core.bool intervalFetch, core.String limit, core.String notification, core.String order, core.String pageToken, core.List<core.String> regionIds}) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (categoryId != null) {
      _queryParams["category_id"] = [categoryId];
    }
    if (intervalFetch != null) {
      _queryParams["interval_fetch"] = ["${intervalFetch}"];
    }
    if (limit != null) {
      _queryParams["limit"] = [limit];
    }
    if (notification != null) {
      _queryParams["notification"] = [notification];
    }
    if (order != null) {
      _queryParams["order"] = [order];
    }
    if (pageToken != null) {
      _queryParams["pageToken"] = [pageToken];
    }
    if (regionIds != null) {
      _queryParams["region_ids"] = regionIds;
    }

    _url = 'feeds/list';

    var _response = _requester.request(_url,
                                       "GET",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new FeedCollection.fromJson(data));
  }

  /**
   * [request] - The metadata request object.
   *
   * Request parameters:
   *
   * [id] - null
   *
   * Completes with a [Feed].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<Feed> patch(FeedProtoRegionIdsCategoryIdQueryObjIntervalFetchNotificationDescriptionTitleStatus request, core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (request != null) {
      _body = convert.JSON.encode((request).toJson());
    }
    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'feeds/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "PATCH",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new Feed.fromJson(data));
  }

  /**
   * [request] - The metadata request object.
   *
   * Request parameters:
   *
   * [id] - null
   *
   * Completes with a [Feed].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<Feed> update(FeedProtoRegionIdsCategoryIdQueryObjIntervalFetchNotificationDescriptionTitleStatus request, core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (request != null) {
      _body = convert.JSON.encode((request).toJson());
    }
    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'feeds/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "POST",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new Feed.fromJson(data));
  }

}


class PostsResourceApi {
  final commons.ApiRequester _requester;

  PostsResourceApi(commons.ApiRequester client) : 
      _requester = client;

  /**
   * Request parameters:
   *
   * [parent] - null
   *
   * [id] - null
   *
   * Completes with a [PostProtoIdTitle].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<PostProtoIdTitle> delete(core.String parent, core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (parent == null) {
      throw new core.ArgumentError("Parameter parent is required.");
    }
    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'posts/' + commons.Escaper.ecapeVariable('$parent') + '/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "DELETE",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new PostProtoIdTitle.fromJson(data));
  }

  /**
   * Request parameters:
   *
   * [parent] - null
   *
   * [id] - null
   *
   * Completes with a [Post].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<Post> get(core.String parent, core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (parent == null) {
      throw new core.ArgumentError("Parameter parent is required.");
    }
    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'posts/' + commons.Escaper.ecapeVariable('$parent') + '/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "GET",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new Post.fromJson(data));
  }

  /**
   * [request] - The metadata request object.
   *
   * Request parameters:
   *
   * [parent] - null
   *
   * [id] - null
   *
   * Completes with a [Post].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<Post> insert(PostProtoEmailTitleLinkLocationPhoneSourceStrUpdatedImgLinkPhotoDescriptionStrPublished request, core.String parent, core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (request != null) {
      _body = convert.JSON.encode((request).toJson());
    }
    if (parent == null) {
      throw new core.ArgumentError("Parameter parent is required.");
    }
    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'posts/' + commons.Escaper.ecapeVariable('$parent') + '/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "PUT",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new Post.fromJson(data));
  }

  /**
   * Request parameters:
   *
   * [parent] - null
   *
   * [email] - null
   *
   * [limit] - null
   *
   * [order] - null
   *
   * [pageToken] - null
   *
   * [phone] - null
   *
   * Completes with a [PostCollection].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<PostCollection> list(core.String parent, {core.String email, core.String limit, core.String order, core.String pageToken, core.String phone}) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (parent == null) {
      throw new core.ArgumentError("Parameter parent is required.");
    }
    if (email != null) {
      _queryParams["email"] = [email];
    }
    if (limit != null) {
      _queryParams["limit"] = [limit];
    }
    if (order != null) {
      _queryParams["order"] = [order];
    }
    if (pageToken != null) {
      _queryParams["pageToken"] = [pageToken];
    }
    if (phone != null) {
      _queryParams["phone"] = [phone];
    }

    _url = 'posts/' + commons.Escaper.ecapeVariable('$parent');

    var _response = _requester.request(_url,
                                       "GET",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new PostCollection.fromJson(data));
  }

  /**
   * [request] - The metadata request object.
   *
   * Request parameters:
   *
   * [parent] - null
   *
   * [id] - null
   *
   * Completes with a [Post].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<Post> update(PostProtoTitleDescriptionPhotoEmailPhone request, core.String parent, core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (request != null) {
      _body = convert.JSON.encode((request).toJson());
    }
    if (parent == null) {
      throw new core.ArgumentError("Parameter parent is required.");
    }
    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'posts/' + commons.Escaper.ecapeVariable('$parent') + '/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "POST",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new Post.fromJson(data));
  }

}


class RegionsResourceApi {
  final commons.ApiRequester _requester;

  RegionsResourceApi(commons.ApiRequester client) : 
      _requester = client;

  /**
   *  Delete a Datastore entity
   *
   * Request parameters:
   *
   * [id] - null
   *
   * Completes with a [RegionProtoIdTitle].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<RegionProtoIdTitle> delete(core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'regions/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "DELETE",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new RegionProtoIdTitle.fromJson(data));
  }

  /**
   *  Get a Datastore entity
   *
   * Request parameters:
   *
   * [id] - null
   *
   * Completes with a [RegionProtoIdTitleDescriptionSubdomainSubdomainOnly].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<RegionProtoIdTitleDescriptionSubdomainSubdomainOnly> get(core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'regions/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "GET",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new RegionProtoIdTitleDescriptionSubdomainSubdomainOnly.fromJson(data));
  }

  /**
   *  Insert a Datastore entity
   *
   * [request] - The metadata request object.
   *
   * Request parameters:
   *
   * [id] - null
   *
   * Completes with a [Region].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<Region> insert(RegionProtoDescriptionTitleSubdomainOnlySubdomain request, core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (request != null) {
      _body = convert.JSON.encode((request).toJson());
    }
    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'regions/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "PUT",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new Region.fromJson(data));
  }

  /**
   *  List Datastore entities
   *
   * Request parameters:
   *
   * [limit] - null
   *
   * [order] - null
   *
   * [pageToken] - null
   *
   * [subdomain] - null
   *
   * [subdomainOnly] - null
   *
   * Completes with a [RegionCollection].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<RegionCollection> list({core.String limit, core.String order, core.String pageToken, core.String subdomain, core.bool subdomainOnly}) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (limit != null) {
      _queryParams["limit"] = [limit];
    }
    if (order != null) {
      _queryParams["order"] = [order];
    }
    if (pageToken != null) {
      _queryParams["pageToken"] = [pageToken];
    }
    if (subdomain != null) {
      _queryParams["subdomain"] = [subdomain];
    }
    if (subdomainOnly != null) {
      _queryParams["subdomain_only"] = ["${subdomainOnly}"];
    }

    _url = 'regions/list';

    var _response = _requester.request(_url,
                                       "GET",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new RegionCollection.fromJson(data));
  }

  /**
   *  Update a Datastore entity
   *
   * [request] - The metadata request object.
   *
   * Request parameters:
   *
   * [id] - null
   *
   * Completes with a [Region].
   *
   * Completes with a [commons.ApiRequestError] if the API endpoint returned an
   * error.
   *
   * If the used [http.Client] completes with an error when making a REST call,
   * this method will complete with the same error.
   */
  async.Future<Region> update(Region request, core.String id) {
    var _url = null;
    var _queryParams = new core.Map();
    var _uploadMedia = null;
    var _uploadOptions = null;
    var _downloadOptions = commons.DownloadOptions.Metadata;
    var _body = null;

    if (request != null) {
      _body = convert.JSON.encode((request).toJson());
    }
    if (id == null) {
      throw new core.ArgumentError("Parameter id is required.");
    }

    _url = 'regions/' + commons.Escaper.ecapeVariable('$id');

    var _response = _requester.request(_url,
                                       "POST",
                                       body: _body,
                                       queryParams: _queryParams,
                                       uploadOptions: _uploadOptions,
                                       uploadMedia: _uploadMedia,
                                       downloadOptions: _downloadOptions);
    return _response.then((data) => new Region.fromJson(data));
  }

}



class Category {
  core.String description;
  core.String id;
  core.String title;

  Category();

  Category.fromJson(core.Map _json) {
    if (_json.containsKey("description")) {
      description = _json["description"];
    }
    if (_json.containsKey("id")) {
      id = _json["id"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (description != null) {
      _json["description"] = description;
    }
    if (id != null) {
      _json["id"] = id;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class CategoryCollection {
  core.List<Category> items;
  core.String nextPageToken;

  CategoryCollection();

  CategoryCollection.fromJson(core.Map _json) {
    if (_json.containsKey("items")) {
      items = _json["items"].map((value) => new Category.fromJson(value)).toList();
    }
    if (_json.containsKey("nextPageToken")) {
      nextPageToken = _json["nextPageToken"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (items != null) {
      _json["items"] = items.map((value) => (value).toJson()).toList();
    }
    if (nextPageToken != null) {
      _json["nextPageToken"] = nextPageToken;
    }
    return _json;
  }
}

class CategoryProtoDescriptionTitle {
  core.String description;
  core.String title;

  CategoryProtoDescriptionTitle();

  CategoryProtoDescriptionTitle.fromJson(core.Map _json) {
    if (_json.containsKey("description")) {
      description = _json["description"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (description != null) {
      _json["description"] = description;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class CategoryProtoIdTitle {
  core.String id;
  core.String title;

  CategoryProtoIdTitle();

  CategoryProtoIdTitle.fromJson(core.Map _json) {
    if (_json.containsKey("id")) {
      id = _json["id"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (id != null) {
      _json["id"] = id;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class CategoryProtoIdTitleDescription {
  core.String description;
  core.String id;
  core.String title;

  CategoryProtoIdTitleDescription();

  CategoryProtoIdTitleDescription.fromJson(core.Map _json) {
    if (_json.containsKey("description")) {
      description = _json["description"];
    }
    if (_json.containsKey("id")) {
      id = _json["id"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (description != null) {
      _json["description"] = description;
    }
    if (id != null) {
      _json["id"] = id;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class Feed {
  core.String categoryId;
  core.String description;
  core.String id;
  core.bool intervalFetch;
  /**
   *
   * Possible string values are:
   * - "NONE"
   * - "EMAIL"
   * - "TEXT"
   * - "BOTH"
   */
  core.String notification;
  Query queryObj;
  core.List<core.String> regionIds;
  core.String status;
  core.String title;

  Feed();

  Feed.fromJson(core.Map _json) {
    if (_json.containsKey("category_id")) {
      categoryId = _json["category_id"];
    }
    if (_json.containsKey("description")) {
      description = _json["description"];
    }
    if (_json.containsKey("id")) {
      id = _json["id"];
    }
    if (_json.containsKey("interval_fetch")) {
      intervalFetch = _json["interval_fetch"];
    }
    if (_json.containsKey("notification")) {
      notification = _json["notification"];
    }
    if (_json.containsKey("query_obj")) {
      queryObj = new Query.fromJson(_json["query_obj"]);
    }
    if (_json.containsKey("region_ids")) {
      regionIds = _json["region_ids"];
    }
    if (_json.containsKey("status")) {
      status = _json["status"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (categoryId != null) {
      _json["category_id"] = categoryId;
    }
    if (description != null) {
      _json["description"] = description;
    }
    if (id != null) {
      _json["id"] = id;
    }
    if (intervalFetch != null) {
      _json["interval_fetch"] = intervalFetch;
    }
    if (notification != null) {
      _json["notification"] = notification;
    }
    if (queryObj != null) {
      _json["query_obj"] = (queryObj).toJson();
    }
    if (regionIds != null) {
      _json["region_ids"] = regionIds;
    }
    if (status != null) {
      _json["status"] = status;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class FeedCollection {
  core.List<Feed> items;
  core.String nextPageToken;

  FeedCollection();

  FeedCollection.fromJson(core.Map _json) {
    if (_json.containsKey("items")) {
      items = _json["items"].map((value) => new Feed.fromJson(value)).toList();
    }
    if (_json.containsKey("nextPageToken")) {
      nextPageToken = _json["nextPageToken"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (items != null) {
      _json["items"] = items.map((value) => (value).toJson()).toList();
    }
    if (nextPageToken != null) {
      _json["nextPageToken"] = nextPageToken;
    }
    return _json;
  }
}

class FeedProtoIdTitle {
  core.String id;
  core.String title;

  FeedProtoIdTitle();

  FeedProtoIdTitle.fromJson(core.Map _json) {
    if (_json.containsKey("id")) {
      id = _json["id"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (id != null) {
      _json["id"] = id;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class FeedProtoIdTitleDescriptionCategoryIdRegionIdsQueryObjIntervalFetchNotificationStatus {
  core.String categoryId;
  core.String description;
  core.String id;
  core.bool intervalFetch;
  /**
   *
   * Possible string values are:
   * - "NONE"
   * - "EMAIL"
   * - "TEXT"
   * - "BOTH"
   */
  core.String notification;
  Query queryObj;
  core.List<core.String> regionIds;
  core.String status;
  core.String title;

  FeedProtoIdTitleDescriptionCategoryIdRegionIdsQueryObjIntervalFetchNotificationStatus();

  FeedProtoIdTitleDescriptionCategoryIdRegionIdsQueryObjIntervalFetchNotificationStatus.fromJson(core.Map _json) {
    if (_json.containsKey("category_id")) {
      categoryId = _json["category_id"];
    }
    if (_json.containsKey("description")) {
      description = _json["description"];
    }
    if (_json.containsKey("id")) {
      id = _json["id"];
    }
    if (_json.containsKey("interval_fetch")) {
      intervalFetch = _json["interval_fetch"];
    }
    if (_json.containsKey("notification")) {
      notification = _json["notification"];
    }
    if (_json.containsKey("query_obj")) {
      queryObj = new Query.fromJson(_json["query_obj"]);
    }
    if (_json.containsKey("region_ids")) {
      regionIds = _json["region_ids"];
    }
    if (_json.containsKey("status")) {
      status = _json["status"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (categoryId != null) {
      _json["category_id"] = categoryId;
    }
    if (description != null) {
      _json["description"] = description;
    }
    if (id != null) {
      _json["id"] = id;
    }
    if (intervalFetch != null) {
      _json["interval_fetch"] = intervalFetch;
    }
    if (notification != null) {
      _json["notification"] = notification;
    }
    if (queryObj != null) {
      _json["query_obj"] = (queryObj).toJson();
    }
    if (regionIds != null) {
      _json["region_ids"] = regionIds;
    }
    if (status != null) {
      _json["status"] = status;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class FeedProtoRegionIdsCategoryIdQueryObjIntervalFetchNotificationDescriptionTitleStatus {
  core.String categoryId;
  core.String description;
  core.bool intervalFetch;
  /**
   *
   * Possible string values are:
   * - "NONE"
   * - "EMAIL"
   * - "TEXT"
   * - "BOTH"
   */
  core.String notification;
  Query queryObj;
  core.List<core.String> regionIds;
  core.String status;
  core.String title;

  FeedProtoRegionIdsCategoryIdQueryObjIntervalFetchNotificationDescriptionTitleStatus();

  FeedProtoRegionIdsCategoryIdQueryObjIntervalFetchNotificationDescriptionTitleStatus.fromJson(core.Map _json) {
    if (_json.containsKey("category_id")) {
      categoryId = _json["category_id"];
    }
    if (_json.containsKey("description")) {
      description = _json["description"];
    }
    if (_json.containsKey("interval_fetch")) {
      intervalFetch = _json["interval_fetch"];
    }
    if (_json.containsKey("notification")) {
      notification = _json["notification"];
    }
    if (_json.containsKey("query_obj")) {
      queryObj = new Query.fromJson(_json["query_obj"]);
    }
    if (_json.containsKey("region_ids")) {
      regionIds = _json["region_ids"];
    }
    if (_json.containsKey("status")) {
      status = _json["status"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (categoryId != null) {
      _json["category_id"] = categoryId;
    }
    if (description != null) {
      _json["description"] = description;
    }
    if (intervalFetch != null) {
      _json["interval_fetch"] = intervalFetch;
    }
    if (notification != null) {
      _json["notification"] = notification;
    }
    if (queryObj != null) {
      _json["query_obj"] = (queryObj).toJson();
    }
    if (regionIds != null) {
      _json["region_ids"] = regionIds;
    }
    if (status != null) {
      _json["status"] = status;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

/**
 * ProtoRPC container for GeoPt instances.
 *
 *   Attributes:
 *     lat: Float; The latitude of the point.
 *     lon: Float; The longitude of the point.
 */
class GeoPtMessage {
  core.double lat;
  core.double lon;

  GeoPtMessage();

  GeoPtMessage.fromJson(core.Map _json) {
    if (_json.containsKey("lat")) {
      lat = _json["lat"];
    }
    if (_json.containsKey("lon")) {
      lon = _json["lon"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (lat != null) {
      _json["lat"] = lat;
    }
    if (lon != null) {
      _json["lon"] = lon;
    }
    return _json;
  }
}

class Post {
  core.String description;
  core.String email;
  core.String id;
  core.String imgLink;
  core.String link;
  /**
   * ProtoRPC container for GeoPt instances.
   *
   *   Attributes:
   *     lat: Float; The latitude of the point.
   *     lon: Float; The longitude of the point.
   */
  GeoPtMessage location;
  core.String parent;
  core.String phone;
  core.String photo;
  core.List<core.int> get photoAsBytes {
    return convert.BASE64.decode(photo);
  }

  void set photoAsBytes(core.List<core.int> _bytes) {
    photo = convert.BASE64.encode(_bytes).replaceAll("/", "_").replaceAll("+", "-");
  }
  core.String source;
  core.String strPublished;
  core.String strUpdated;
  core.String title;

  Post();

  Post.fromJson(core.Map _json) {
    if (_json.containsKey("description")) {
      description = _json["description"];
    }
    if (_json.containsKey("email")) {
      email = _json["email"];
    }
    if (_json.containsKey("id")) {
      id = _json["id"];
    }
    if (_json.containsKey("img_link")) {
      imgLink = _json["img_link"];
    }
    if (_json.containsKey("link")) {
      link = _json["link"];
    }
    if (_json.containsKey("location")) {
      location = new GeoPtMessage.fromJson(_json["location"]);
    }
    if (_json.containsKey("parent")) {
      parent = _json["parent"];
    }
    if (_json.containsKey("phone")) {
      phone = _json["phone"];
    }
    if (_json.containsKey("photo")) {
      photo = _json["photo"];
    }
    if (_json.containsKey("source")) {
      source = _json["source"];
    }
    if (_json.containsKey("str_published")) {
      strPublished = _json["str_published"];
    }
    if (_json.containsKey("str_updated")) {
      strUpdated = _json["str_updated"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (description != null) {
      _json["description"] = description;
    }
    if (email != null) {
      _json["email"] = email;
    }
    if (id != null) {
      _json["id"] = id;
    }
    if (imgLink != null) {
      _json["img_link"] = imgLink;
    }
    if (link != null) {
      _json["link"] = link;
    }
    if (location != null) {
      _json["location"] = (location).toJson();
    }
    if (parent != null) {
      _json["parent"] = parent;
    }
    if (phone != null) {
      _json["phone"] = phone;
    }
    if (photo != null) {
      _json["photo"] = photo;
    }
    if (source != null) {
      _json["source"] = source;
    }
    if (strPublished != null) {
      _json["str_published"] = strPublished;
    }
    if (strUpdated != null) {
      _json["str_updated"] = strUpdated;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class PostCollection {
  core.List<Post> items;
  core.String nextPageToken;

  PostCollection();

  PostCollection.fromJson(core.Map _json) {
    if (_json.containsKey("items")) {
      items = _json["items"].map((value) => new Post.fromJson(value)).toList();
    }
    if (_json.containsKey("nextPageToken")) {
      nextPageToken = _json["nextPageToken"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (items != null) {
      _json["items"] = items.map((value) => (value).toJson()).toList();
    }
    if (nextPageToken != null) {
      _json["nextPageToken"] = nextPageToken;
    }
    return _json;
  }
}

class PostProtoEmailTitleLinkLocationPhoneSourceStrUpdatedImgLinkPhotoDescriptionStrPublished {
  core.String description;
  core.String email;
  core.String imgLink;
  core.String link;
  /**
   * ProtoRPC container for GeoPt instances.
   *
   *   Attributes:
   *     lat: Float; The latitude of the point.
   *     lon: Float; The longitude of the point.
   */
  GeoPtMessage location;
  core.String phone;
  core.String photo;
  core.List<core.int> get photoAsBytes {
    return convert.BASE64.decode(photo);
  }

  void set photoAsBytes(core.List<core.int> _bytes) {
    photo = convert.BASE64.encode(_bytes).replaceAll("/", "_").replaceAll("+", "-");
  }
  core.String source;
  core.String strPublished;
  core.String strUpdated;
  core.String title;

  PostProtoEmailTitleLinkLocationPhoneSourceStrUpdatedImgLinkPhotoDescriptionStrPublished();

  PostProtoEmailTitleLinkLocationPhoneSourceStrUpdatedImgLinkPhotoDescriptionStrPublished.fromJson(core.Map _json) {
    if (_json.containsKey("description")) {
      description = _json["description"];
    }
    if (_json.containsKey("email")) {
      email = _json["email"];
    }
    if (_json.containsKey("img_link")) {
      imgLink = _json["img_link"];
    }
    if (_json.containsKey("link")) {
      link = _json["link"];
    }
    if (_json.containsKey("location")) {
      location = new GeoPtMessage.fromJson(_json["location"]);
    }
    if (_json.containsKey("phone")) {
      phone = _json["phone"];
    }
    if (_json.containsKey("photo")) {
      photo = _json["photo"];
    }
    if (_json.containsKey("source")) {
      source = _json["source"];
    }
    if (_json.containsKey("str_published")) {
      strPublished = _json["str_published"];
    }
    if (_json.containsKey("str_updated")) {
      strUpdated = _json["str_updated"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (description != null) {
      _json["description"] = description;
    }
    if (email != null) {
      _json["email"] = email;
    }
    if (imgLink != null) {
      _json["img_link"] = imgLink;
    }
    if (link != null) {
      _json["link"] = link;
    }
    if (location != null) {
      _json["location"] = (location).toJson();
    }
    if (phone != null) {
      _json["phone"] = phone;
    }
    if (photo != null) {
      _json["photo"] = photo;
    }
    if (source != null) {
      _json["source"] = source;
    }
    if (strPublished != null) {
      _json["str_published"] = strPublished;
    }
    if (strUpdated != null) {
      _json["str_updated"] = strUpdated;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class PostProtoIdTitle {
  core.String id;
  core.String title;

  PostProtoIdTitle();

  PostProtoIdTitle.fromJson(core.Map _json) {
    if (_json.containsKey("id")) {
      id = _json["id"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (id != null) {
      _json["id"] = id;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class PostProtoTitleDescriptionPhotoEmailPhone {
  core.String description;
  core.String email;
  core.String phone;
  core.String photo;
  core.List<core.int> get photoAsBytes {
    return convert.BASE64.decode(photo);
  }

  void set photoAsBytes(core.List<core.int> _bytes) {
    photo = convert.BASE64.encode(_bytes).replaceAll("/", "_").replaceAll("+", "-");
  }
  core.String title;

  PostProtoTitleDescriptionPhotoEmailPhone();

  PostProtoTitleDescriptionPhotoEmailPhone.fromJson(core.Map _json) {
    if (_json.containsKey("description")) {
      description = _json["description"];
    }
    if (_json.containsKey("email")) {
      email = _json["email"];
    }
    if (_json.containsKey("phone")) {
      phone = _json["phone"];
    }
    if (_json.containsKey("photo")) {
      photo = _json["photo"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (description != null) {
      _json["description"] = description;
    }
    if (email != null) {
      _json["email"] = email;
    }
    if (phone != null) {
      _json["phone"] = phone;
    }
    if (photo != null) {
      _json["photo"] = photo;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class Query {
  core.bool bundleDuplicates;
  core.List<core.String> condition;
  core.bool hasPic;
  core.String maxPrice;
  core.String minPrice;
  core.String postal;
  core.bool postedToday;
  core.String queryString;
  core.String searchDistance;
  core.bool searchNearby;
  core.bool srchType;

  Query();

  Query.fromJson(core.Map _json) {
    if (_json.containsKey("bundle_duplicates")) {
      bundleDuplicates = _json["bundle_duplicates"];
    }
    if (_json.containsKey("condition")) {
      condition = _json["condition"];
    }
    if (_json.containsKey("has_pic")) {
      hasPic = _json["has_pic"];
    }
    if (_json.containsKey("max_price")) {
      maxPrice = _json["max_price"];
    }
    if (_json.containsKey("min_price")) {
      minPrice = _json["min_price"];
    }
    if (_json.containsKey("postal")) {
      postal = _json["postal"];
    }
    if (_json.containsKey("posted_today")) {
      postedToday = _json["posted_today"];
    }
    if (_json.containsKey("query_string")) {
      queryString = _json["query_string"];
    }
    if (_json.containsKey("search_distance")) {
      searchDistance = _json["search_distance"];
    }
    if (_json.containsKey("search_nearby")) {
      searchNearby = _json["search_nearby"];
    }
    if (_json.containsKey("srch_type")) {
      srchType = _json["srch_type"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (bundleDuplicates != null) {
      _json["bundle_duplicates"] = bundleDuplicates;
    }
    if (condition != null) {
      _json["condition"] = condition;
    }
    if (hasPic != null) {
      _json["has_pic"] = hasPic;
    }
    if (maxPrice != null) {
      _json["max_price"] = maxPrice;
    }
    if (minPrice != null) {
      _json["min_price"] = minPrice;
    }
    if (postal != null) {
      _json["postal"] = postal;
    }
    if (postedToday != null) {
      _json["posted_today"] = postedToday;
    }
    if (queryString != null) {
      _json["query_string"] = queryString;
    }
    if (searchDistance != null) {
      _json["search_distance"] = searchDistance;
    }
    if (searchNearby != null) {
      _json["search_nearby"] = searchNearby;
    }
    if (srchType != null) {
      _json["srch_type"] = srchType;
    }
    return _json;
  }
}

class Region {
  core.String description;
  core.String id;
  core.String subdomain;
  core.bool subdomainOnly;
  core.String title;

  Region();

  Region.fromJson(core.Map _json) {
    if (_json.containsKey("description")) {
      description = _json["description"];
    }
    if (_json.containsKey("id")) {
      id = _json["id"];
    }
    if (_json.containsKey("subdomain")) {
      subdomain = _json["subdomain"];
    }
    if (_json.containsKey("subdomain_only")) {
      subdomainOnly = _json["subdomain_only"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (description != null) {
      _json["description"] = description;
    }
    if (id != null) {
      _json["id"] = id;
    }
    if (subdomain != null) {
      _json["subdomain"] = subdomain;
    }
    if (subdomainOnly != null) {
      _json["subdomain_only"] = subdomainOnly;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class RegionCollection {
  core.List<Region> items;
  core.String nextPageToken;

  RegionCollection();

  RegionCollection.fromJson(core.Map _json) {
    if (_json.containsKey("items")) {
      items = _json["items"].map((value) => new Region.fromJson(value)).toList();
    }
    if (_json.containsKey("nextPageToken")) {
      nextPageToken = _json["nextPageToken"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (items != null) {
      _json["items"] = items.map((value) => (value).toJson()).toList();
    }
    if (nextPageToken != null) {
      _json["nextPageToken"] = nextPageToken;
    }
    return _json;
  }
}

class RegionProtoDescriptionTitleSubdomainOnlySubdomain {
  core.String description;
  core.String subdomain;
  core.bool subdomainOnly;
  core.String title;

  RegionProtoDescriptionTitleSubdomainOnlySubdomain();

  RegionProtoDescriptionTitleSubdomainOnlySubdomain.fromJson(core.Map _json) {
    if (_json.containsKey("description")) {
      description = _json["description"];
    }
    if (_json.containsKey("subdomain")) {
      subdomain = _json["subdomain"];
    }
    if (_json.containsKey("subdomain_only")) {
      subdomainOnly = _json["subdomain_only"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (description != null) {
      _json["description"] = description;
    }
    if (subdomain != null) {
      _json["subdomain"] = subdomain;
    }
    if (subdomainOnly != null) {
      _json["subdomain_only"] = subdomainOnly;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class RegionProtoIdTitle {
  core.String id;
  core.String title;

  RegionProtoIdTitle();

  RegionProtoIdTitle.fromJson(core.Map _json) {
    if (_json.containsKey("id")) {
      id = _json["id"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (id != null) {
      _json["id"] = id;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}

class RegionProtoIdTitleDescriptionSubdomainSubdomainOnly {
  core.String description;
  core.String id;
  core.String subdomain;
  core.bool subdomainOnly;
  core.String title;

  RegionProtoIdTitleDescriptionSubdomainSubdomainOnly();

  RegionProtoIdTitleDescriptionSubdomainSubdomainOnly.fromJson(core.Map _json) {
    if (_json.containsKey("description")) {
      description = _json["description"];
    }
    if (_json.containsKey("id")) {
      id = _json["id"];
    }
    if (_json.containsKey("subdomain")) {
      subdomain = _json["subdomain"];
    }
    if (_json.containsKey("subdomain_only")) {
      subdomainOnly = _json["subdomain_only"];
    }
    if (_json.containsKey("title")) {
      title = _json["title"];
    }
  }

  core.Map<core.String, core.Object> toJson() {
    final core.Map<core.String, core.Object> _json = new core.Map<core.String, core.Object>();
    if (description != null) {
      _json["description"] = description;
    }
    if (id != null) {
      _json["id"] = id;
    }
    if (subdomain != null) {
      _json["subdomain"] = subdomain;
    }
    if (subdomainOnly != null) {
      _json["subdomain_only"] = subdomainOnly;
    }
    if (title != null) {
      _json["title"] = title;
    }
    return _json;
  }
}
