import 'package:polymer/polymer.dart';
import 'dart:html' show Event, Node, CustomEvent;
import 'item_element.dart' show ItemElement;	// TODO: , ClientLib;
import 'clientlib.dart' as ClientLib;
import 'dart:mirrors';


@CustomTag('feed-element')
class FeedElement extends ItemElement {

  final itemType = "Feed";

  // TODO: Can access Feed as ItemElement.Endpoint.Feed?
  final apiType = reflectClass(ClientLib.Feed);

  FeedElement.created() : super.created();

  /// List of regions for form. Passed in via element attribute.
  @observable List<Region> regions;

  /// List of regions for form. Passed in via element attribute.
  @observable List<Category> categories;

 /// More info for form
  String _regionTitle() {
    for(var region in regions) {
      if(region.id == item.regionId) {
        return region.title;
      }
    }
  }

  /// Convenience property for use in form
  String get regionTitle => _regionTitle();

  String _categoryTitle() {
    for(var category in categories) {
      if(category.id == item.categoryId) {
        return category.title;
      }
    }
  }

  /// Convenience property for use in form
  String get categoryTitle => _categoryTitle();

 /// More info for form
  String _regionTitles() {
    List<String> _titles = [];
    for(var region in regions) {
      if(item.regionIds.contains(region.id)) {
        _titles.add(region.title);
      }
    }
    return _titles.join(', ');
  }

  /// Convenience property for use in form
  String get regionTitles => _regionTitles();

  /// Convenience property for use in form
  String get condition => _condition();

  /// TODO: Determ if condition ever null
  String _condition() {
    List qobj = item.queryObj.condition;
    if (qobj == null)
      return "";
    else
      return qobj.join(', ');
  }

  void updateItem(Event e, var detail, Node sender) {
    super.updateItem(e, detail, sender);
    /// Update Item subclass values here.
    if (cachedItem.categoryId != item.categoryId) {
      /// Feed's category has changed. Dispatch a
      /// custom event to allow the element's parent
      /// (listener) to update the filtered items list.
      /// TODO: Rename event from legacy 'levelchanged'
      ///       to 'filterchanged' or 'categorychanged'
      dispatchEvent(new CustomEvent('levelchanged'));
    }
  }

  void cancelEditing(Event e, var detail, Node sender) {
    super.cancelEditing(e, detail, sender);
    /// Restore unmodified Item subclass values here.
  }

  void startEditing(Event e, var detail, Node sender) {
    super.startEditing(e, detail, sender);
    /// Cache Item subclass values here.
  }

  void deleteItem(Event e, var detail, Node sender) {
    super.deleteItem(e, detail, sender);
    /// Perform any needed Item subclass operations here.
  }

  /// Copies values from source feed to destination feed.
  void copyItem(source, destination) {
    super.copyItem(source, destination);
    /// Copy Item subclass values here.
    destination.regionIds  = source.regionIds;
    destination.categoryId = source.categoryId;
    destination.queryObj   = source.queryObj;
    destination.intervalFetch = source.intervalFetch;
    destination.notification = source.notification;
    destination.status = source.status;
  }
}
