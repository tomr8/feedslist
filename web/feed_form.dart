import 'package:polymer/polymer.dart';
import 'model.dart' show Feed, Region, Category, Query, Search;
import 'dart:html' show CustomEvent, Event, Node;
import 'item_form.dart' show ItemFormElement;

/*
 * The class for creating or updating a feed. Performs validation based on
 * a feed based on validation rules defined in the model.
 */
@CustomTag('feed-form')
class FeedFormElement extends ItemFormElement {

  /// List of regions for form. Passed in via element attribute.
  @observable List<Region> regions;

  /// List of categories for form. Passed in via element attribute.
  @observable List<Category> categories;

  FeedFormElement.created() : super.created() {}

  /// Conforms to template in ItemForm
  /// NOTE: can also use validation in HTML
  bool validatePrice() {
    // Erased input changes value to string
    var minPrice = item.queryObj.minPrice;
    var maxPrice = item.queryObj.maxPrice;

    // Erased input may have changed null to string
    if (minPrice == "")
      minPrice = null;
    if (maxPrice == "")
      item.queryObj.maxPrice = null;

    try {
      return item.queryObj.minPrice <= item.queryObj.maxPrice;
    }
    catch(NoSuchMethodError) {
      // acceptably null min or max price
      return true;
    }
  }

  bool validateNotification() {
    if (item.notification == "") {
      // Erased input turned into emptring
      item.notification = null;
    }
    // TODO: Validate against valid options
    return true;
  }

  /// TODO: Abstract these `toggle...` methods to toggleBoolean(<field name>).
  ///       May need to use reflection.
  ///       Or else find a way to bind the checkbox state to its property.
  void toggleIntervalFetch() {
    item.intervalFetch = !(item.intervalFetch);
  }

  void toggleSearchNearby() {
    item.queryObj.searchNearby = !(item.queryObj.searchNearby);
  }

  void togglePostedToday() {
    item.queryObj.postedToday = !(item.queryObj.postedToday);
  }

  /// TODO: Toggle when query string field de/populated
  void toggleSrchType() {
    item.queryObj.srchType = !(item.queryObj.srchType);
  }

  void toggleHasPic() {
    item.queryObj.hasPic = !(item.queryObj.hasPic);
  }

  void toggleBundleDuplicates() {
    item.queryObj.bundleDuplicates = !(item.queryObj.bundleDuplicates);
  }

  // TODO: Validate searchDistance.It must be int,
  //       but gets set to "" when previously-entered
  //       value is removed.

  // TODO: Validate postal.It must be int, but
  //       gets set to "" when previously-entered
  //       value is removed.

  // Parse into list of strings
  // TODO: Determ reasonable way to set condition
  //       other than in validation method
  bool validateCondition() {
    var qobj = item.queryObj;
    if(qobj.condition is String) {
      // Check if condition a string (set by user)
      qobj.condition = qobj.condition.split(',');

      // "".split(' ') == 1
      if (qobj.condition.length == 1 && qobj.condition[0] == '')
          qobj.condition = [];
    }
    else if(qobj.condition == null)
      qobj.condition = [];
    // TODO: Has already been assigned to? Where?

    // Handle input spaces. A valid condition will never have one.
    //
    for(int i = 0; i < qobj.condition.length; i++)
      qobj.condition[i] = qobj.condition[i].trim();

    /// TODO: Validate as enums
//  for (var c in cnd {
//    try {
//      Condition(c);
//    except {
//      /// not valid Condition
//    }
//  }
    return true;
  }

  // Parse into list of strings
  // TODO: See notes in validateCondition
  bool validateRegionIds() {
    if(item.regionIds is String) {
      // Check if condition a string (set by user)
      item.regionIds = item.regionIds.split(',');

      // "".split(',') == 1
      if (item.regionIds.length == 1 && item.regionIds[0] == '')
          item.regionIds = [];
    }
    else if(item.regionIds == null)
      item.regionIds = [];

    // Handle input spaces. A valid condition will never have one.
    //
    for(int i = 0; i < item.regionIds.length; i++)
      item.regionIds[i] = item.regionIds[i].trim();

    return true;
  }
  /// Required definition. Do nothing if possible
//void set condition(String cond) => cond;

  void validateItem(Event event, Object detail, Node sender) {
    /// Superclass validation dispatches custom 'itemvalidated' event
    /// for which the parent (...-list) class registers a listener.
    /// Perform any needed Item subclass validations here, first.

    /// TODO: Validate "searchNearby"
    if (validatePrice() &&
        validateCondition() &&
        validateNotification() &&
        validateRegionIds()) {
      super.validateItem(event, detail, sender);
    }
  }

  void cancelForm(Event event, Object detail, Node sender) {
    /// Superclass clears title and description, dispatches
    /// custom 'formnotneeded' event for parent.
    /// Perform any needed subclass operations here.
    super.cancelForm(event, detail, sender);
  }
}
