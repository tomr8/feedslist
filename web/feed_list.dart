import 'package:polymer/polymer.dart';
import 'item.dart' show Item;
import 'item_list.dart' show ItemList;	// , RestApi;
import 'dart:html' show Event, Node;
import 'clientlib.dart' as ClientLib;
import 'dart:mirrors';

/// Class to represent a collection of Feed objects.
@CustomTag('feed-list')
class FeedList extends ItemList {
  final itemType = 'Feed';

  final apiType = reflectClass(ClientLib.Feed);

  static const ALL = "all";

  /// List of filter values. Includes the levels defined in the model, as well
  /// as a filter to return all feeds.
  @observable List<String> filters = toObservable([ALL]);

  /// String that stores the value used to filter feeds.
  @observable String filterValue = ALL;

  /// The list of filtered feeds.
  @observable List<Feed> filteredItems = toObservable([]);

  /// The list of regions
  @observable List<Region> regions = toObservable([]);

  /// The list of regions
  @observable List<Category> categories = toObservable([]);

  void resetForm() {
    /// Insert needed initializations here
    super.resetForm();
  }

  void addItem(Event e, var detail, Node sender) {
    /// Insert needed initializations here
    super.addItem(e, detail, sender);
  }

  void deleteItem(Event e, var detail, Node sender) {
    /// Insert needed teardown here
    super.deleteItem(e, detail, sender);
  }

  void copyItem(source, destination) {
    /// Copy Item subclass values here.
    destination.regionIds  = source.regionIds;
    destination.categoryId = source.categoryId;
    destination.queryObj   = source.queryObj;
    destination.intervalFetch = source.intervalFetch;
    destination.notification = source.notification;
    destination.status = source.status;
    super.copyItem(source, destination);
  }

  /// Calculates the feeds to display when using a filter.
  /// TODO: consider moving to base class
  void filter() {
    if (filterValue == ALL) {
      filteredItems = items;
    }
    else {
      filteredItems = items.where((item) {
        return item.categoryId == filterValue;
      }).toList();
    }
  }

  /// Refreshes the filtered feeds list every time the feeds list changes.
  /// TODO: consider moving to base class
  void itemsChanged() {
    filter();
  }

  /// Named constructor. Sets initial value of filtered feeds and sets
  /// the new feed's level to the default.
  FeedList.created() : super.created() {
    filteredItems = items;
    // TODO: figure out the defaultLevel thing
//  newItem.regionId = defaultLevel;

    /// Get an authenticated client and call `list()`
    var client = getClient().then((client) {
        api = new ClientLib.FeedslistApi(client);
        resourceApi = api.feeds;

        // Get the categories
        api.categories.list().then((response) {
            if (response.items == null)
              results = "Fetched no categories";
            else {
              for(var category in response.items) {
                categories.add(category);
                filters.add(category.id);
              }
              results = "Fetched ${response.items.length} categories";
            }
        });

        // Get the regions
        api.regions.list().then((response) {
            if (response.items == null)
              results = "Fetched no regions";
            else {
              for(var region in response.items) {
                regions.add(region);
              }
              results = "Fetched ${response.items.length} regions";
            }
        });

        /// TODO: Need Feed::source? Specify fields subset.
        results = "Fetching feeds...";
        api.feeds.list().then((response) {
            if (response.items == null)
              results = "Fetched no feeds";
            else {
              for(var respItem in response.items) {
                Item item = new Item(itemType, '', '', '');
                copyItem(respItem, item);
                items.add(item);
              }
              results = "Fetched ${response.items.length} feeds";
            }
        });
    });
  }
}
