import 'package:polymer/polymer.dart';
import 'dart:mirrors';


class Item extends Observable {
  String _id;
  String _title;
  String _description;

  static const MAX_ID_LENGTH          =  40;  // 40-char hash value in Post
  static const MIN_TITLE_LENGTH       =  4;
  static const MAX_TITLE_LENGTH       = 100;
  static const MAX_DESCRIPTION_LENGTH = 500;

  String get id => _id;
  set id(String s) => _id = s;

  String get title => _title;
  set title(String s) => _title = s;

  String get description => _description;
  set description(String s) => _description = s;

   /// Allow a subclass instance to be generated with
   /// an empty argument list
  Item.created(id, title, description) {
    // NOTE: Can't use the shorthand initializers
    // Workaround (hack) for issue noted on 17 May
    this.id = id;
    this.title = title;
    this.description = description;
  }

  factory Item(String type, String id, String title, String description) {
    if(type == null)
      return; // Todo: Determ how this is being called w/ no type
    MirrorSystem libs = currentMirrorSystem();
    LibraryMirror lib = libs.findLibrary(new Symbol('polymer_and_dart.web.models'));
    Map<Symbol, Mirror> classes = lib.declarations;
    ClassMirror cls = classes[new Symbol(type)];
    InstanceMirror inst = cls.newInstance(new Symbol(''), [id, title, description]);
    return inst.reflectee;
  }
}
