import 'package:polymer/polymer.dart';
import 'item.dart' show Item;
import 'dart:html' show Event, Node, CustomEvent;
import "package:googleapis_auth/auth_browser.dart";
import 'dart:mirrors';

@CustomTag('item-element')
class ItemElement extends PolymerElement {
  @published Item item;
  @observable bool editing = false;
  Item _cachedItem;

  /// Reference to model class name. Set by subclass.
  final String itemType;  // e.g., "Post"

  /// Reference to endpoints model name. Set by subclass.
  /// Used to create API request object
  ClassMirror apiType;  // e.g., "ClientLib.Codelab"

  /// Reference to subclass-specific endpoints service (resource)
  /// Passed into subclass via element attribute
  // NOTE: formerly named `resourceApi`, which was problematic
  // due to XML attribute-naming scheme
  var api;

  /// Variable for status of API calls
  /// Initialized in ItemList.
  /// Passed into ItemElement subclass via element attribute
  @observable String results;

  /// convenience method
  bool nullOrEmpty(var val) {
    return val == null || val.isEmpty;
  }

  /// convenience method
  bool nullOrZero(var val) {
    return val == null || val == '0';
  }

  /// Make private variable accessible by subclasses
  Item get cachedItem => _cachedItem;
  set cachedItem(item) => _cachedItem = item;

  /// Dispatch custom event for element's parent to handle.
  void edit(Event e, var detail, Node sender) {
    dispatchEvent(new CustomEvent('startedit',
        detail: {'item': item}));
  }

  /// Dispatch custom event for element's parent to handle.
  void delete(Event e, var detail, Node sender) {
    dispatchEvent(new CustomEvent('delete',
        detail: {'item': item}));
  }

  /// Updates item.
  void updateItem(Event e, var detail, Node sender) {
    e.preventDefault();
    editing = false;
    // TODO: Modify form so id can't be changed
    item.id = cachedItem.id; // Prevent id from being changed
    /// EPD methods take an instance of the back end (NDB) model. It's
    /// a little strange.
    var request = apiType.newInstance(new Symbol(''), []).reflectee;
    copyItem(detail['item'], request);

    /// No auth flow needed. Woo hoo!
    // TODO: Why no auth flow needed?
    // TODO: Consider fixing this hack. How to properly parameterize
    //       methods with variable parameter lists?
    // Upadate item's values for any computed (alias) properties
    // which may have changed on the back end as response to
    // other attr(s) being updated
    if (apiType.instanceMembers.containsKey(new Symbol('parent'))) {
      // parent required
      api.update(request, item.parent, item.id).then((response) {
          copyItem(response, item);
          results = "Updated item: ${response.title}";
      })
      .catchError((e) {
//        cancelEditing();
          results = "${e.status}: ${e.message}";
      });
    } else {
      api.update(request, item.id).then((response) {
          copyItem(response, item);
          results = "Updated item: ${response.title}";
      })
      .catchError((e) {
          results = "${e.status}: ${e.message}";
      });
    }
  }

  /// Cancels editing, restoring the original item values.
  void cancelEditing(Event e, var detail, Node sender) {
    e.preventDefault();
    copyItem(_cachedItem, item);
    editing = false;
  }

  /// Starts editing, caching the item values.
  void startEditing(Event e, var detail, Node sender) {
    e.preventDefault();
    copyItem(item, _cachedItem);
    editing = true;
  }

  /// Dispatches a custom event requesting the item be deleted.
  void deleteItem(Event e, var detail, Node sender) {
    e.preventDefault();
    dispatchEvent(new CustomEvent('deleteitem',
        detail: {'item': item}));
  }

  /// Copies values from source item to destination item.
  void copyItem(source, destination) {
    destination.id = source.id;
    destination.title = source.title;
    destination.description = source.description;
  }

  ItemElement.created() : super.created() {
    _cachedItem = new Item(itemType, '', '', '');
  }
}
