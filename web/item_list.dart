import 'package:polymer/polymer.dart';
import 'item.dart' show Item;
import 'dart:html' show Event, Node;
import "package:googleapis_auth/auth_browser.dart";
import 'clientlib.dart' show RestApi;

// TODO: Read from config file
const CLIENT_ID = '324354282878-88jtdjf3377a2eoppog5gnb2lenhrg5a.apps.googleusercontent.com';
const CLIENT_SECRET = '6VRVjBJIO7Olm9k6hujo8UYH'; // Is this used?

/// Class to represent a collection of Item objects.
@CustomTag('item-list')
class ItemList extends PolymerElement {
  // endpoints services root
  RestApi api;

  // endpoints class to create for requests
  ClassMirror apiType; // Codelab, Course, etc

  // Specific endpoints service (api.codelabs, etc). A 'resource' so named in
  // the generated client library. Value set in ItemList subclass.
  var resourceApi;

  /// Field for a new Item object.
  @observable Item newItem;

  // TODO: document this
  final String itemType;

  /// The canonical collection of items in this app.
  @observable List<Item> items = toObservable([]);

  @observable String results = "results";

  /// Show/hide "Add new" form
  @observable bool showForm = false;

  void toggleShowForm(Event e, var detail, Node sender) {
    showForm = !showForm;
  }

  /// Replaces the existing new Item, causing the new item form to reset.
  void resetForm() {
    newItem = new Item(itemType, '', '', '');
  }

  /// Adds an item to the items list and resets the new item form.
  /// This triggers itemsChanged().
  /// TODO: May need parent for some subclasses (see deleteItem).
  /// NOTE: No parent required as posts not added in web client
  void addItem(Event e, var detail, Node sender) {
    e.preventDefault();
    var item = detail['item'];
    var request = apiType.newInstance(new Symbol(''), []);
    request = request.reflectee;
    copyItem(item, request);
    results = "Adding ${item.title}...";
    /// Add to datastore
    resourceApi.insert(request, id=request.id).then((response) {
        items.add(item);
        resetForm();  /// Dismiss form only after success
        toggleShowForm(e, detail, sender);
        results = "Added item: ${response.title}";
    }).catchError((e) {
        results = "${e.status}: ${e.message}";
    });
  }

  /// Removes an item from the items list. This triggers itemsChanged().
  void deleteItem(Event e, var detail, Node sender) {
    var item = detail['item'];

    // TODO: Determ how to pass optional `parent`. Made more
    // difficult by its being 1st param.
    if (apiType.instanceMembers.containsKey(new Symbol('parent'))) {
      resourceApi.delete(item.parent, item.id).then((response) {
          results = "Deleted item: ${response.title}";
          items.remove(item);
      })
      .catchError((e) {
          results = "${e.status}: ${e.message}";
      });
    } else {
      resourceApi.delete(item.id).then((response) {
          results = "Deleted item: ${response.title}";
          items.remove(item);
      })
      .catchError((e) {
          results = "${e.status}: ${e.message}";
      });
    }
  }

  /// Copies values from source item to destination item.
  void copyItem(source, destination) {
    destination.id = source.id;
    destination.title = source.title;
    destination.description = source.description;
  }

  /// Return the API for this application
  /// Must be in the base class of the outermost element
  /// in which it is used?
  Future getClient() async {
    // TODO: set clientId in subclasses
    var clientId = new ClientId(CLIENT_ID, null);
    var scopes = ['https://www.googleapis.com/auth/userinfo.email'];

    var flow = await createImplicitBrowserFlow(clientId, scopes);
    var client = await flow.clientViaUserConsent();
//  client.close(); // TODO: When is this necessary?
//  flow.close();
    return client;
  }

  /// Named constructor. Sets title and description to
  /// empty strings.
  ItemList.created() : super.created() {
    newItem = new Item(itemType, '', '', '');
  }
}
