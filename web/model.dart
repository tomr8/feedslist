library polymer_and_dart.web.models;

import 'package:polymer/polymer.dart';
import 'item.dart';
import 'clientlib.dart' as Clientlib;


// Subobject of Feed
//
class Query {

  @observable int minPrice;
  @observable int maxPrice;
  @observable bool searchNearby = false;
  @observable String postal;
  @observable int searchDistance;
  @observable List<String> condition = toObservable([]);
  @observable bool postedToday = false;
  @observable bool srchType = false;
  @observable bool hasPic = false;
  @observable String queryString;
  @observable bool bundleDuplicates = false;
}

// TODO: Remove if unneeded
class Photo extends Item {

  @observable String postId;
  @observable String image;

  Photo(id, title, description) : super.created(id, title, description);
}

class Category extends Item {

  Category(id, title, description) : super.created(id, title, description);
}

class Region extends Item {

  @observable String subdomain;
  @observable bool subdomainOnly;

  Region(id, title, description) : super.created(id, title, description);
}

class Feed extends Item {
/// Here for example of setting static const class var
//static const List<String> LEVELS = const [
//      'easy', 'intermediate', 'advanced'];

  static const List<String> NOTIFICATIONS = const ['TEXT', 'EMAIL'];

  @observable String categoryId;
  @observable String description;
  @observable bool intervalFetch = false;
  ///Possible string values are:
  ///BOTH * EMAIL * NONE * TEXT
  @observable String notification;
  // Use Request class Query having the toJSON method.
  @observable Clientlib.Query queryObj;
  @observable List<String> regionIds = toObservable([]);
  @observable String status;

  Feed(id, title, description) : super.created(id, title, description) {
    // Populate this for newItem displayed in FeedList form
    // TODO: Does this conflict with instantiating Feed::query_obj on back end?
    queryObj = new Clientlib.Query();
  }
}

class Post extends Item {
  /// These seem pretty reasonable for email
  static const MIN_EMAIL_LENGTH = 7;
  static const MAX_EMAIL_LENGTH = 50;
  static const MAX_PHONE_LENGTH = 50;

  @observable String email;
  @observable String feedId;
  @observable String imgLink;
  @observable String link;
  @observable String parent;
  @observable String photo;
  @observable String phone;
  @observable String strPublished;
  @observable String strUpdated;

  Post(id, title, description) : super.created(id, title, description);
}
