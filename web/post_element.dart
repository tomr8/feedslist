import 'package:polymer/polymer.dart';
import 'dart:html' show Event, Node, CustomEvent;
import 'item_element.dart' show ItemElement;
import 'clientlib.dart' as ClientLib;
import 'dart:mirrors';
import 'model.dart' show Photo;


@CustomTag('post-element')
class PostElement extends ItemElement {

  /// Used to instantiate class from model.dart
  final itemType = "Post";

  /// ClassMirror declared in ItemElement. Used by ItemElement
  /// to create back-end model instances as request objects.
  /// 'ClientLib.' disambiguates it from front-end class of same name.
  final apiType = reflectClass(ClientLib.Post);

  PostElement.created() : super.created();

  /// List of feeds for form. Passed in via element attribute.
  @observable List<Feed> feeds;

  /// More info for form
  String _feedTitle() {
    for(var feed in feeds) {
      if(feed.id == item.parent) {
        return feed.title;
      }
    }
  }

  /// Convenience property - no need for method call in form
  String get feedTitle => _feedTitle();

  /*
  /// Failed attempt to populate UI field asynchronously
  String _imageData() async {
    /// Get the string of bytes for display with Post
    // TODO: Find out how to use a query param
    // TODO: Broken. If anything is returned, it's a Future that doesn't
    // resolve into the desired return value.
    // TODO: Learn asynchronous problemming.
//  api.photos.list('ecf209d5d1c716878d9503c9c5c7d559ac84ca2f').then((result) {
    // TODO: Check out the closure problem, here, wherein the returned image
    // seems to be always the same.

    /// Prefix image data for display in <img>
    var prefix = 'data:image/png;base64,';
    /// Display alternative "red dot"
    var redDot = 'iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==';

    var retVal = "$prefix ${redDot}"; // default image
    var result = await api.photos.list(item.id);

    try {
        // Just need the first one
      var image = result.items[0].image;
      retVal = "$prefix $image";
    // TODO: handle null items
    } finally {
      return retVal;
    }
  }
  */

  /// Access data inside photo
  String _imageData() {
    // These are JPEG images. How, then, do they display correctly?
    var prefix = 'data:image/png;base64,';
    /// Display alternative "red dot"
    var redDot = 'iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==';

//  var retVal = "$prefix ${redDot}"; // default image
    var retVal = "$prefix ${item.photo}";  // actual image

    return retVal;
  }

  /// Convenience property - no need for method call in form
  String get imageData => _imageData();

  void updateItem(Event e, var detail, Node sender) {
    super.updateItem(e, detail, sender);
    /// Update Item subclass values here.
    // TODO: The post's feed will never change.
    //        Make the property const in the API.
    if (cachedItem.feedId != item.feedId) {
      /// Post's feed has changed. Dispatch a
      /// custom event to allow the element's parent to register
      /// a listener to update the filtered posts list.
      dispatchEvent(new CustomEvent('feedchanged'));
    }
  }

  void cancelEditing(Event e, var detail, Node sender) {
    super.cancelEditing(e, detail, sender);
    /// Restore unmodified Item subclass values here.
  }

  void startEditing(Event e, var detail, Node sender) {
    super.startEditing(e, detail, sender);
    /// Cache Item subclass values here.
  }

  void deleteItem(Event e, var detail, Node sender) {
    super.deleteItem(e, detail, sender);
    /// Perform any needed Item subclass operations here.
  }

  /// Copies values from source post to destination post.
  void copyItem(source, destination) {
    super.copyItem(source, destination);
    /// Copy Item subclass values here.
    destination.parent = source.parent;
    destination.photo  = source.photo;
    destination.email  = source.email;
    destination.phone  = source.phone;
    destination.imgLink= source.imgLink;
    destination.link   = source.link;
    // TODO: Why did I comment these out?
//  destination.strPublished = source.strPublished;
//  destination.strUpdated = source.strUpdated;
  }
}
