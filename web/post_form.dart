import 'package:polymer/polymer.dart';
import 'model.dart' show Post, Feed; // TODO: else Exception: There is no library named 'polymer_and_dart.web.models'
import 'dart:html' show CustomEvent, Event, Node;
import 'item_form.dart' show ItemFormElement;

/*
 * The class for creating or updating a post. Performs validation based on
 * a post based on validation rules defined in the model.
 */
@CustomTag('post-form')
class PostFormElement extends ItemFormElement {

  /// Getters that make Item static values accessible in the template.
  int get minEmailLength => Post.MIN_EMAIL_LENGTH;
  int get maxEmailLength => Post.MAX_EMAIL_LENGTH;
  int get maxPhoneLength => Post.MAX_PHONE_LENGTH;

  /// Variables used in displaying error messages.
  @observable String emailErrorMessage = '';
  @observable String phoneErrorMessage = '';

  /// Variables used in displaying chars remaining messages.
  @observable int emailCharsLeft = Post.MAX_EMAIL_LENGTH;
  @observable int phoneCharsLeft = Post.MAX_PHONE_LENGTH;

  /// List of feeds for form. Passed in via element attribute.
  @observable List<Feed> feeds;

  PostFormElement.created() : super.created() {}

  /// Validates the item's email
  final RegExp emailRegEx = new RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');

  /// Validates the item's phone (no extensions)
  final RegExp phoneRegEx = new RegExp(r'^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})$');

  /// Validates the item's phone (with extensions)
//final String phoneRegEx = new RegExp(r'^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$');

  bool validateEmail() {
    emailCharsLeft = maxEmailLength - item.email.length;
    /*
    if (item.email.length < minEmailLength ||
        item.email.length > maxEmailLength) {
      emailErrorMessage = "Email must be between $minEmailLength and "
          "$maxEmailLength characters.";
      return false;
    }
    */

    if (item.email.length > maxEmailLength ||
        emailRegEx.hasMatch(item.email) == false) {
    /// from S/O: 16800540/validate-email-address-in-dart
      emailErrorMessage = "Must be valid email.";
      return false;
    }

    emailErrorMessage = '';
    return true;
  }

  /// Validates the item's phone
  bool validatePhone() {
    if (phoneRegEx.hasMatch(item.phone) == false) {
    /// from S/O: 16800540/validate-email-address-in-dart
      phoneErrorMessage = "Must be valid phone.";
      return false;
    }

    phoneErrorMessage = '';
    return true;
  }

  void validateItem(Event event, Object detail, Node sender) {
    /// Superclass validation dispatches custom 'itemvalidated' event
    /// for which the parent (...-list) class registers a listener.
    if (validateEmail())  /// E.g.: item.level == "easy"
      super.validateItem(event, detail, sender);
      /// Perform any needed Item subclass validations here, first.
      /// Valid email? phone?
  }

  void cancelForm(Event event, Object detail, Node sender) {
    /// Superclass clears title and description, dispatches
    /// custom 'formnotneeded' event for parent.
    /// Perform any needed subclass operations here.
    super.cancelForm(event, detail, sender);
  }
}
