import 'package:polymer/polymer.dart';
import 'item.dart'      show Item;
import 'item_list.dart' show ItemList;
import 'dart:html'      show Event, Node, window;
import 'clientlib.dart' as ClientLib;
import 'dart:mirrors';

/// Class to represent a collection of Post objects.
@CustomTag('post-list')
class PostList extends ItemList {
  final itemType = 'Post';

  final apiType = reflectClass(ClientLib.Post);

  // TODO: remove when unnecessary
  /// Passed to PostElement through template attr
  var photosApi;

  static const ALL = 'all';

  /// List of filter values. Appended to in ctor.
  @observable List<String> filters = toObservable([ALL]);

  /// String that stores the value used to filter codelabs.
  @observable String filterValue = ALL;

  /// The list of filtered codelabs.
  @observable List<Post> filteredItems = toObservable([]);

  /// The list of feeds
  @observable List<Feed> feeds = toObservable([]);

  /// `list` param limiting number of query results
  @observable String limit = '1';

  /// List of limit values (endpoints max is 10)
  @observable List<String> limits = toObservable(['1', '3', '5', '10']);

  /// Page tokens for `list` method. '' to hide link
  @observable String nextPageToken = '';

  void resetForm() {
    /// Insert needed initializations here
    super.resetForm();
  }

  /// No addItem need in web client as posts added
  /// only via client script

  void deleteItem(Event e, var detail, Node sender) {
    /// Insert needed teardown here
    super.deleteItem(e, detail, sender);
  }

  void copyItem(source, destination) {
    destination.parent = source.parent;
    destination.photo  = source.photo;
    destination.email  = source.email;
    destination.phone  = source.phone;
    destination.imgLink= source.imgLink;
    destination.link   = source.link;
    destination.strPublished = source.strPublished;
    destination.strUpdated = source.strUpdated;
    super.copyItem(source, destination);
  }


  /// Calculates the codelabs to display when using a filter.
  /// TODO: consider moving to base class
  void filter() {
    if (filterValue == ALL) {
      filteredItems = items;
    }
    else {
      filteredItems = items.where((item) {
        return item.parent == filterValue;
      }).toList();
    }
  }

  /// Refreshes the filtered codelabs list every time the codelabs list changes.
  /// TODO: consider moving to base class
  void itemsChanged() {
    filter();
  }

  /// Fetch items from API based on state of filters in UI
  // TODO: Implement token list so can go back and forth
  void nextPage() {
    /* Get the next page of results */

    /// Get `limit` from the URL query params
    String limitParam = limit;
    /// `null` is OK, 'all' not.
    if (limit == ALL) {
      limitParam = null;
    }

    /// Get `filterValue` from the URL query params
    String filterParam = filterValue;
    if (filterValue == ALL) {
      filterParam = null;
    }

    /// TODO: Write up how this design came about
    /// TODO: use resourceApi
    /// Execute a query with the pageToken param
    results = "Fetching posts...";
    api.posts.list(filterParam,
                   pageToken: nextPageToken,
                       limit: limitParam,
                       order: '-published'
    ).then((response) {
        /// Update 'next'
        nextPageToken = response.nextPageToken;
        /// Handle end of list
        if (nextPageToken == null) {
          /// Enable 'isEmpty' in template
          nextPageToken = '';
        }

        items.clear();

        if (response.items != null) {
          for(var respItem in response.items) {
            Item item = new Item(itemType, '', '', '');
            copyItem(respItem, item);
            items.add(item);
          }
          results = "Fetched ${response.items.length} posts";
        }
        else
          results = "Fetched no posts";
    });
  }

  /// Named constructor. Sets initial value of filtered codelabs and sets
  /// the new codelab's level to the default.
  PostList.created() : super.created() {
    filteredItems = items;

    // TODO: figure out the defaultLevel thing
//  newItem.regionId = defaultLevel;

    /// Get an authenticated client and call `list()`
    var client = getClient().then((client) {

        api = new ClientLib.FeedslistApi(client);

        resourceApi = api.posts;

        /// Get the feeds first for use by posts
        api.feeds.list().then((response) {
            for(var feed in response.items) {
              feeds.add(feed);
              filters.add(feed.id);
            }
        });
    });
  }
}
