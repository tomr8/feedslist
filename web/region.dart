//library polymer_and_dart.web.models;

import 'package:polymer/polymer.dart';
import 'item.dart';

/// The barebones model for a codelab. Defines constants used for validation.
class Region extends Item {

  Region(id, title, description) : super.created(id, title, description);
}
