import 'package:polymer/polymer.dart';
import 'dart:html' show Event, Node, CustomEvent;
import 'item_element.dart' show ItemElement;	// TODO: , ClientLib;
import 'clientlib.dart' as ClientLib;
import 'dart:mirrors';


@CustomTag('region-element')
class RegionElement extends ItemElement {

  final itemType = "Region";

  // TODO: Can access Region as ItemElement.Endpoint.Region?
  final apiType = reflectClass(ClientLib.Region);

  RegionElement.created() : super.created();

  void updateItem(Event e, var detail, Node sender) {
    super.updateItem(e, detail, sender);
    /// Update Item subclass values here.
  }

  void cancelEditing(Event e, var detail, Node sender) {
    super.cancelEditing(e, detail, sender);
    /// Restore unmodified Item subclass values here.
  }

  void startEditing(Event e, var detail, Node sender) {
    super.startEditing(e, detail, sender);
    /// Cache Item subclass values here.
  }

  void deleteItem(Event e, var detail, Node sender) {
    super.deleteItem(e, detail, sender);
    /// Perform any needed Item subclass operations here.
  }

  /// Copies values from source Region to destination Region.
  void copyItem(source, destination) {
    super.copyItem(source, destination);
    /// Copy Item subclass values here.
    destination.subdomain = source.subdomain;
    destination.subdomainOnly = source.subdomainOnly;
  }
}
