import 'package:polymer/polymer.dart';
import 'item.dart' show Item;
import 'item_list.dart' show ItemList;	// , RestApi;
import 'dart:html' show Event, Node;
import 'model.dart' show Region;
import 'clientlib.dart' as ClientLib;
import 'dart:mirrors';

/// Class to represent a collection of Region objects.
@CustomTag('region-list')
class RegionList extends ItemList {
  final itemType = 'Region';

  final apiType = reflectClass(ClientLib.Region);

  static const ALL = "all";

  /// List of filter values. Includes the levels defined in the model, as well
  /// as a filter to return all regions.
  @observable List<String> filters = toObservable([ALL]);

  /// String that stores the value used to filter regions.
  @observable String filterValue = ALL;

  /// The list of filtered regions.
  @observable List<Region> filteredItems = toObservable([]);

  void resetForm() {
    /// Insert needed initializations here
    super.resetForm();
  }

  void addItem(Event e, var detail, Node sender) {
    /// Insert needed initializations here
    super.addItem(e, detail, sender);
  }

  void deleteItem(Event e, var detail, Node sender) {
    /// Insert needed teardown here
    super.deleteItem(e, detail, sender);
  }

  void copyItem(source, destination) {
    super.copyItem(source, destination);
    /// Copy Item subclass values here.
    destination.subdomain = source.subdomain;
    destination.subdomainOnly = source.subdomainOnly;
  }

  /// Calculates the regions to display when using a filter.
  void filter() {
    filteredItems = items;
  }

  /// Refreshes the filtered regions list every time the regions list changes.
  /// TODO: consider moving to base class
  void itemsChanged() {
    filter();
  }

  /// Named constructor. Sets initial value of filtered regions and sets
  /// the new region's level to the default.
  RegionList.created() : super.created() {
    filteredItems = items;
    // TODO: figure out the defaultLevel thing
//  newItem.regionId = defaultLevel;

    /// Get an authenticated client and call `list()`
    var client = getClient().then((client) {
        api = new ClientLib.FeedslistApi(client);
        resourceApi = api.regions;

        results = "Fetching regions...";
        api.regions.list().then((response) {
            if (response.items == null)
              results = "Fetched no regions";
            else {
              for(var respItem in response.items) {
                Item item = new Item(itemType, '', '', '');
                copyItem(respItem, item);
                items.add(item);
              }
              results = "Fetched ${response.items.length} regions";
            }
        });
    });
  }
}
